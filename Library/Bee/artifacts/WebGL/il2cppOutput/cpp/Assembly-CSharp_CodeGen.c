﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.AudioSource RandomPlayer::get_source()
extern void RandomPlayer_get_source_m255EF513B1E0CC9D744C4E7D2F7EC2ACE8EDE686 (void);
// 0x00000002 System.Void RandomPlayer::Awake()
extern void RandomPlayer_Awake_m20C900D30753BC37A250A9914F2C0F58EB8DC0CD (void);
// 0x00000003 UnityEngine.AudioClip RandomPlayer::GetRandomClip()
extern void RandomPlayer_GetRandomClip_m097681BEF8B2D7685254C499D15FC4CBE9DA336A (void);
// 0x00000004 System.Void RandomPlayer::PlayRandom()
extern void RandomPlayer_PlayRandom_m46B2095625A2D86225BC1A9E35BF5F31DA2D903B (void);
// 0x00000005 System.Void RandomPlayer::PlayClip(UnityEngine.AudioClip,System.Single,System.Single)
extern void RandomPlayer_PlayClip_mB797975091AF55EC23545DEF90C2EF446261ECE5 (void);
// 0x00000006 System.Void RandomPlayer::.ctor()
extern void RandomPlayer__ctor_m75DAC2A102EDA3B588BB016226795206680637E7 (void);
// 0x00000007 UIAudioPlayer UIAudioPlayer::get_Instance()
extern void UIAudioPlayer_get_Instance_mEFA2C5F53C86DAAD2E3F3A87DF3473E0D4CFAC05 (void);
// 0x00000008 System.Void UIAudioPlayer::set_Instance(UIAudioPlayer)
extern void UIAudioPlayer_set_Instance_m17CD95BE5758E9E62930B0D8DCF13328DCB6922C (void);
// 0x00000009 System.Void UIAudioPlayer::Awake()
extern void UIAudioPlayer_Awake_m94912E521D386330DA83A7E2320C730F6D92A292 (void);
// 0x0000000A System.Void UIAudioPlayer::PlayPositive()
extern void UIAudioPlayer_PlayPositive_mFD7EC92A738D86664BAC4933C73B5DD5BB7DDBF4 (void);
// 0x0000000B System.Void UIAudioPlayer::PlayNegative()
extern void UIAudioPlayer_PlayNegative_m8EC01980081EA09B5D677D8530AAA276301FA4B6 (void);
// 0x0000000C System.Void UIAudioPlayer::.ctor()
extern void UIAudioPlayer__ctor_m392A9F7A93243476B1A554D9E473A53CDF53ADC6 (void);
// 0x0000000D System.Void WorldAudioPool::Awake()
extern void WorldAudioPool_Awake_mD0AA692A8A922BA89881F0913D72C2D5258A3BAF (void);
// 0x0000000E System.Void WorldAudioPool::Init()
extern void WorldAudioPool_Init_m4C4DBEE92DCF2C61628840D3025DEB2D625DAB39 (void);
// 0x0000000F UnityEngine.AudioSource WorldAudioPool::GetWorldSFXSource()
extern void WorldAudioPool_GetWorldSFXSource_m2C4BF9380E7C454AC09787B2D698DAAC8394A148 (void);
// 0x00000010 System.Void WorldAudioPool::.ctor()
extern void WorldAudioPool__ctor_mB731A9349B3830C2BDAB5455D55A629BC22C2504 (void);
// 0x00000011 CameraShaker CameraShaker::get_Instance()
extern void CameraShaker_get_Instance_m9AAD23D1C29F9766C20354D474EC939D0281730D (void);
// 0x00000012 System.Void CameraShaker::set_Instance(CameraShaker)
extern void CameraShaker_set_Instance_m71F7E168A9EB1D5ADA275D7F61D7FCA4AF18CC43 (void);
// 0x00000013 System.Void CameraShaker::Awake()
extern void CameraShaker_Awake_m7BF082AA73D6159B01E22FE07935670BAC0545EA (void);
// 0x00000014 System.Void CameraShaker::Update()
extern void CameraShaker_Update_m0168605AE22B1672350A6A94966649A653F00655 (void);
// 0x00000015 System.Void CameraShaker::Shake(System.Single,System.Single)
extern void CameraShaker_Shake_m22A0C6974F2877F9BE6B6555C1A8775E92D7144F (void);
// 0x00000016 System.Void CameraShaker::.ctor()
extern void CameraShaker__ctor_m302CB9C4B3D79B3D8E67A68E91F9383348B44891 (void);
// 0x00000017 ImpactManager ImpactManager::get_Instance()
extern void ImpactManager_get_Instance_m7981ADB40706C439E5B984E94ACEB5C8C1509FD1 (void);
// 0x00000018 System.Void ImpactManager::set_Instance(ImpactManager)
extern void ImpactManager_set_Instance_m2C8165FDAEA813F2798CF85640609DCCBF8024A3 (void);
// 0x00000019 System.Void ImpactManager::Awake()
extern void ImpactManager_Awake_mFAA328BB40CFE935BF7D6276D524E71A6DEE4745 (void);
// 0x0000001A System.Void ImpactManager::Start()
extern void ImpactManager_Start_mF0F6F179FADD52A235D5031AFB3CA145BB05AFFE (void);
// 0x0000001B System.Void ImpactManager::PlayImpact(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Material)
extern void ImpactManager_PlayImpact_mB8A8290E212FCA8D5FF2DD5E0A2DBB15AD1317EC (void);
// 0x0000001C System.Void ImpactManager::.ctor()
extern void ImpactManager__ctor_m1C000330332AD9116F08AD5353F4F1A28AA4DBB5 (void);
// 0x0000001D System.Void ImpactManager/ImpactSetting::.ctor()
extern void ImpactSetting__ctor_mA704E5F4FEBC43B7B0FDA460C1BF16771845306C (void);
// 0x0000001E System.Void LiquidAmmoDisplay::UpdateAmount(System.Int32,System.Int32)
extern void LiquidAmmoDisplay_UpdateAmount_m278B68AAB87187869E82B0955454EC9525DEB9F5 (void);
// 0x0000001F System.Void LiquidAmmoDisplay::.ctor()
extern void LiquidAmmoDisplay__ctor_m7ABD5576B1FE1008589D22FE2DC2D2CD33D94AB4 (void);
// 0x00000020 System.Void LiquidContainer::Awake()
extern void LiquidContainer_Awake_m4AB75F7622714E43081C32003E2E9716458F7BD0 (void);
// 0x00000021 System.Void LiquidContainer::ChangeLiquidAmount(System.Single)
extern void LiquidContainer_ChangeLiquidAmount_mBCCB8CF030FA0FBE2225024725BFDDDB3B5020BF (void);
// 0x00000022 System.Void LiquidContainer::Update()
extern void LiquidContainer_Update_mAE5BB50E6E162022E7316582A0E9C68D65C48862 (void);
// 0x00000023 System.Void LiquidContainer::.ctor()
extern void LiquidContainer__ctor_m8A09F5F079F9878D05964C398BB1A6636B9AD182 (void);
// 0x00000024 System.Void TextAmmoDisplay::UpdateAmount(System.Int32,System.Int32)
extern void TextAmmoDisplay_UpdateAmount_mA625E78DBD77EE1DE128A24F6ED630B641329820 (void);
// 0x00000025 System.Void TextAmmoDisplay::.ctor()
extern void TextAmmoDisplay__ctor_mE3310C49F1AFA8885169A498AC989E90BA068387 (void);
// 0x00000026 GameDatabase GameDatabase::get_Instance()
extern void GameDatabase_get_Instance_mE05AC3BD084D8B3F9C434530B8BCC54AC3A8218E (void);
// 0x00000027 System.Void GameDatabase::.ctor()
extern void GameDatabase__ctor_mC94FDE78AC36519F4B5A706B0C6740EC4763C9EB (void);
// 0x00000028 AmmoDatabase/Entry AmmoDatabase::GetEntry(System.String)
extern void AmmoDatabase_GetEntry_m6DF800F060D1F1899EB26149E23C952A8FCAC95B (void);
// 0x00000029 AmmoDatabase/Entry AmmoDatabase::GetEntry(System.Int32)
extern void AmmoDatabase_GetEntry_m6C5E57B313250C9198FF3385827FE9DDE5903E36 (void);
// 0x0000002A System.Void AmmoDatabase::.ctor()
extern void AmmoDatabase__ctor_m53114069A022D9B731946CBD88450D6954B755EC (void);
// 0x0000002B System.Void AmmoDatabase/Entry::.ctor()
extern void Entry__ctor_m1C2E8822676326F7DF6F491C32D14331B7C26AB7 (void);
// 0x0000002C System.Void AmmoDatabase/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mB78831EE7C0FE427AA7D303094FCA0E4833DBCEF (void);
// 0x0000002D System.Boolean AmmoDatabase/<>c__DisplayClass4_0::<GetEntry>b__0(AmmoDatabase/Entry)
extern void U3CU3Ec__DisplayClass4_0_U3CGetEntryU3Eb__0_mB16C0D70A20D9F9C0A2ECE752B1839A0BE5495C7 (void);
// 0x0000002E System.Void AmmoDatabase/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mA5AC537E51982A1752621DF0F531C5BA5D60C977 (void);
// 0x0000002F System.Boolean AmmoDatabase/<>c__DisplayClass5_0::<GetEntry>b__0(AmmoDatabase/Entry)
extern void U3CU3Ec__DisplayClass5_0_U3CGetEntryU3Eb__0_m78B9ED31A1276B5FFC579ABBCFB10DC3D5000538 (void);
// 0x00000030 System.Void Episode::.ctor()
extern void Episode__ctor_m20423B23EB6A8A2EB71D2B4F9001493599498BCE (void);
// 0x00000031 System.Void GradientTexture::.ctor()
extern void GradientTexture__ctor_m6E78726316E8C19C00BF643E91EE03E0D0675DAD (void);
// 0x00000032 UnityEngine.Vector3[] PathSystem::get_worldNode()
extern void PathSystem_get_worldNode_m283EC603B5E180BD357F4A134262704A608B7E74 (void);
// 0x00000033 System.Void PathSystem::Init(UnityEngine.Transform)
extern void PathSystem_Init_m815E1596DDA5EAE471ED7706038313410128DEE6 (void);
// 0x00000034 System.Void PathSystem::InitData(PathSystem/PathData)
extern void PathSystem_InitData_m026DB59D4E9B270B7AD581806819958833E73777 (void);
// 0x00000035 PathSystem/PathEvent PathSystem::Move(PathSystem/PathData,System.Single)
extern void PathSystem_Move_m73D5CBEA14BDAEE4033F24D6B4BA4F9C754F0EA9 (void);
// 0x00000036 System.Void PathSystem::.ctor()
extern void PathSystem__ctor_m419DE0D77AD1F951409F2A0C3EA1ECCCE67E21EB (void);
// 0x00000037 System.Void PathSystem/PathData::.ctor()
extern void PathData__ctor_mA45CBE4566016FE84EF18EE1BC93FF4EAE479788 (void);
// 0x00000038 System.Void Helpers::RecursiveLayerChange(UnityEngine.Transform,System.Int32)
extern void Helpers_RecursiveLayerChange_m845740834A96063756F1B5534763F1537FC8E76A (void);
// 0x00000039 System.Void Helpers::.ctor()
extern void Helpers__ctor_mE14820E4C735448B10388645E26A1EE606E86A8B (void);
// 0x0000003A System.Void Key::OnEnable()
extern void Key_OnEnable_m816D2FC38A3D7C9F5D8AF910D7183245633C8ECF (void);
// 0x0000003B System.Void Key::OnTriggerEnter(UnityEngine.Collider)
extern void Key_OnTriggerEnter_m9DB36FB68944E6C173FF7CE7F1393676785FE813 (void);
// 0x0000003C System.Void Key::.ctor()
extern void Key__ctor_mA069FB77FE5BAEC833A31356AB0867315F988954 (void);
// 0x0000003D System.Void Keychain::GrabbedKey(System.String)
extern void Keychain_GrabbedKey_mCE72B9E93D32E9BEBC80F38D716A22FBBBD9A8B0 (void);
// 0x0000003E System.Boolean Keychain::HaveKey(System.String)
extern void Keychain_HaveKey_m920DCA3580AC90AE8030019CB7634180E9421852 (void);
// 0x0000003F System.Void Keychain::UseKey(System.String)
extern void Keychain_UseKey_mA7DAEFBB9A40F590FF322A56F2BC482852A5B493 (void);
// 0x00000040 System.Void Keychain::.ctor()
extern void Keychain__ctor_mFE5336B1256A28FAAAD1F4AEA5270A14532FC667 (void);
// 0x00000041 System.Void Lock::Start()
extern void Lock_Start_mBA33705016890B08069EBE6F715EB8453E5C47B2 (void);
// 0x00000042 System.Void Lock::OnTriggerEnter(UnityEngine.Collider)
extern void Lock_OnTriggerEnter_mCD982438199FD138CC2F8323F221D36ACBBF267C (void);
// 0x00000043 System.Void Lock::OnTriggerExit(UnityEngine.Collider)
extern void Lock_OnTriggerExit_m6CFA56B36A66C401EE6F3C59D2C786D284AB571C (void);
// 0x00000044 System.Void Lock::Opened()
extern void Lock_Opened_mD57A8F76630F23AC79799119FF5C3D2C92EC03BB (void);
// 0x00000045 System.Void Lock::.ctor()
extern void Lock__ctor_m11B2038D869328BCF986F07428B6FCC7EE8DEFFD (void);
// 0x00000046 System.Boolean LevelLayout::get_Destroyed()
extern void LevelLayout_get_Destroyed_mC27334F207CE70D82A801B13B1726B3B73070C6E (void);
// 0x00000047 System.Void LevelLayout::set_Destroyed(System.Boolean)
extern void LevelLayout_set_Destroyed_mB32F773825499ECDC402265D38DC01299BF0FEED (void);
// 0x00000048 System.Void LevelLayout::OnDestroy()
extern void LevelLayout_OnDestroy_m8702D1C5D6A9F863AD7327EB31430194E36C00EE (void);
// 0x00000049 System.Void LevelLayout::.ctor()
extern void LevelLayout__ctor_m2DCC4F7A20D2FCFA0E3ECDB1019D734893D5DDD6 (void);
// 0x0000004A System.Void LevelRoom::Placed(LevelLayout)
extern void LevelRoom_Placed_m335DC1E8BC6A7F311C10FD2F46C47EB471518E21 (void);
// 0x0000004B System.Void LevelRoom::.ctor()
extern void LevelRoom__ctor_m3AC6CC6E6509946452170907A85EB773A87D3F93 (void);
// 0x0000004C System.Void LevelRoomGroup::.ctor()
extern void LevelRoomGroup__ctor_m1189DE70C404AC53303F2D76EAA0615A7F9EC542 (void);
// 0x0000004D System.Collections.Generic.List`1<UnityEngine.Renderer> MinimapElement::get_Renderers()
extern void MinimapElement_get_Renderers_m5B5F9479EC9F627A1772D40FC0FB89B153C726F2 (void);
// 0x0000004E System.Void MinimapElement::OnEnable()
extern void MinimapElement_OnEnable_m53672F3A01196CAF9583F33EBCF185F86039112B (void);
// 0x0000004F System.Void MinimapElement::OnDisable()
extern void MinimapElement_OnDisable_m9716243A9306FB34C4BB92888B4DCBB4BB349DEE (void);
// 0x00000050 System.Void MinimapElement::.ctor()
extern void MinimapElement__ctor_mA06D434E62FA5B2D9666C5C5C5CE2E4CB0439B10 (void);
// 0x00000051 System.Void MinimapElement::.cctor()
extern void MinimapElement__cctor_mF8C9598AA87CCC02FF8B5691815F720443D1FBCA (void);
// 0x00000052 System.Void MinimapSystem::.cctor()
extern void MinimapSystem__cctor_mE9C922A33BD055943F22BB1851A079275A213497 (void);
// 0x00000053 System.Void MinimapSystem::Render(UnityEngine.RenderTexture,UnityEngine.Vector3,UnityEngine.Vector3,MinimapSystem/MinimapSystemSetting)
extern void MinimapSystem_Render_mA320EBFF70876AE845D7C45ECDED95A80B1BA429 (void);
// 0x00000054 System.Void MinimapSystem::.ctor()
extern void MinimapSystem__ctor_mD80DC40AE6D7C856B60AB86A013192E7B96AD708 (void);
// 0x00000055 System.Void AmmoBox::Reset()
extern void AmmoBox_Reset_mE0F24551744E709F3442287ED3442BFED9E38C5E (void);
// 0x00000056 System.Void AmmoBox::OnTriggerEnter(UnityEngine.Collider)
extern void AmmoBox_OnTriggerEnter_mCAB57357B5EEAB210C70A4AF9094EAEA8D21D1A9 (void);
// 0x00000057 System.Void AmmoBox::.ctor()
extern void AmmoBox__ctor_m5041D6AC701B7BD9B7BE340E12DB8A8330686F6E (void);
// 0x00000058 System.Void AmmoInventoryEntry::.ctor()
extern void AmmoInventoryEntry__ctor_mE1EB635B5375A539C7B0B2F8A7E352FF1001C7F4 (void);
// 0x00000059 Controller Controller::get_Instance()
extern void Controller_get_Instance_m4C033AEA0A4452523EC3948C1505B1417BC985B0 (void);
// 0x0000005A System.Void Controller::set_Instance(Controller)
extern void Controller_set_Instance_m88C95BE455936719549C754DA7C5486B9C3A1090 (void);
// 0x0000005B System.Single Controller::get_Speed()
extern void Controller_get_Speed_mCE7CC38AC975379497269D1E0AC5BF0855987859 (void);
// 0x0000005C System.Void Controller::set_Speed(System.Single)
extern void Controller_set_Speed_mF91E05548BA70CEB77E4AB4352245DFA9E06DCCF (void);
// 0x0000005D System.Boolean Controller::get_LockControl()
extern void Controller_get_LockControl_m1B590B98AD884F7DA775A9522974715005E1882D (void);
// 0x0000005E System.Void Controller::set_LockControl(System.Boolean)
extern void Controller_set_LockControl_m76FC9E9F865EE9A8DD003EBDF074C40115990BC2 (void);
// 0x0000005F System.Boolean Controller::get_CanPause()
extern void Controller_get_CanPause_m1500FCAA6CA05B5916C48EEC0A0689773EE32803 (void);
// 0x00000060 System.Void Controller::set_CanPause(System.Boolean)
extern void Controller_set_CanPause_mE5F4347A8E644CFCD5CFEDBDA3C9D47E9F1A1653 (void);
// 0x00000061 System.Boolean Controller::get_Grounded()
extern void Controller_get_Grounded_mAFB6D1C160D6E8A94FA2EEA25BEEE92A00711E91 (void);
// 0x00000062 System.Void Controller::Awake()
extern void Controller_Awake_mB1780968013F87D703550B691ADB9A9817150FE4 (void);
// 0x00000063 System.Void Controller::Start()
extern void Controller_Start_m6CF9E52A1D22548B67650167CBA335928CEC4796 (void);
// 0x00000064 System.Void Controller::Update()
extern void Controller_Update_mDE8E185DE18C109FA2E8C6FCBFEB86F100F21634 (void);
// 0x00000065 System.Void Controller::DisplayCursor(System.Boolean)
extern void Controller_DisplayCursor_mD863823D42057267BE1E1FF24C7F6437D9F211E6 (void);
// 0x00000066 System.Void Controller::PickupWeapon(Weapon)
extern void Controller_PickupWeapon_m2457CA969AB446D80FBD5A3C677D79854291ECD0 (void);
// 0x00000067 System.Void Controller::ChangeWeapon(System.Int32)
extern void Controller_ChangeWeapon_mB276822F9046AA92CF79AD7EBFC7408C99A15EFA (void);
// 0x00000068 System.Int32 Controller::GetAmmo(System.Int32)
extern void Controller_GetAmmo_mD9E08D9150B1C342114AE03C7511EECAA4C02D2A (void);
// 0x00000069 System.Void Controller::ChangeAmmo(System.Int32,System.Int32)
extern void Controller_ChangeAmmo_mB7A524B0D0EB728D63DF917A262E02F38030464B (void);
// 0x0000006A System.Void Controller::PlayFootstep()
extern void Controller_PlayFootstep_mC8832354A76947C8D9A10229ADB156A6B5FCB4F2 (void);
// 0x0000006B System.Void Controller::.ctor()
extern void Controller__ctor_mD8A105DFD9CAFDD0B419A5ECD5BC3B789338476A (void);
// 0x0000006C System.Void Controller/<>c__DisplayClass46_0::.ctor()
extern void U3CU3Ec__DisplayClass46_0__ctor_m51D8D10A510BEAF6DDCB82727E77F8BE3F91760F (void);
// 0x0000006D System.Boolean Controller/<>c__DisplayClass46_0::<PickupWeapon>b__0(Weapon)
extern void U3CU3Ec__DisplayClass46_0_U3CPickupWeaponU3Eb__0_m9A19576F2B459803A02C0C712209DCFDC7283301 (void);
// 0x0000006E System.Void EndCheckpoint::OnTriggerEnter(UnityEngine.Collider)
extern void EndCheckpoint_OnTriggerEnter_mD0E98E9442E6C40B7D9F846AD0862FD6162F2AED (void);
// 0x0000006F System.Void EndCheckpoint::.ctor()
extern void EndCheckpoint__ctor_mBCCDDEB1E7AFAD107C9E7474654F110EA90A4390 (void);
// 0x00000070 GameSystem GameSystem::get_Instance()
extern void GameSystem_get_Instance_m95EEFCB95AC313F4828A0A5EB13AE8848AB07C07 (void);
// 0x00000071 System.Void GameSystem::set_Instance(GameSystem)
extern void GameSystem_set_Instance_m9184B3938238957EF57D0DDA53045213264C4AC4 (void);
// 0x00000072 System.Single GameSystem::get_RunTime()
extern void GameSystem_get_RunTime_m4CA3A03B46332D7FC7FC585DD2D01E89446616B6 (void);
// 0x00000073 System.Int32 GameSystem::get_TargetCount()
extern void GameSystem_get_TargetCount_m30571DC7D5F32D51B7EB995B6C534A9C96B92A4D (void);
// 0x00000074 System.Int32 GameSystem::get_DestroyedTarget()
extern void GameSystem_get_DestroyedTarget_mC6760EB74CC37D80D9BCEF6A40CDA874862754FF (void);
// 0x00000075 System.Int32 GameSystem::get_Score()
extern void GameSystem_get_Score_mDAA1F04F61BC57A54C8532EE72DF03EEEB0BD4E3 (void);
// 0x00000076 System.Void GameSystem::Awake()
extern void GameSystem_Awake_m49B2786FF71ACC6EAAC99F840AC0F92E18ADB259 (void);
// 0x00000077 System.Void GameSystem::Start()
extern void GameSystem_Start_mF9E1A5B3DDCD3AF572B2C9251405804B47CF6241 (void);
// 0x00000078 System.Void GameSystem::ResetTimer()
extern void GameSystem_ResetTimer_mA439459F5468ED84DE39F1B0474ABC964618648E (void);
// 0x00000079 System.Void GameSystem::StartTimer()
extern void GameSystem_StartTimer_mA8212E89CC3FFE1436C6F12B4F345492352B483F (void);
// 0x0000007A System.Void GameSystem::StopTimer()
extern void GameSystem_StopTimer_m077F3C5C3F3ABCF19AF0AED8040E99AA6F8DE37B (void);
// 0x0000007B System.Void GameSystem::FinishRun()
extern void GameSystem_FinishRun_m3C616268ECF405A4AB7D8131EAB8F6492C1BBF7D (void);
// 0x0000007C System.Void GameSystem::NextLevel()
extern void GameSystem_NextLevel_mBFE5FCA6C9CB0B7B22076103CA25C00D6B4F2A68 (void);
// 0x0000007D System.Void GameSystem::RetrieveTargetsCount()
extern void GameSystem_RetrieveTargetsCount_mC7AD75F15F0465EDF5D215AC4B0823E6B17C2700 (void);
// 0x0000007E System.Void GameSystem::Update()
extern void GameSystem_Update_m2AC59D669C6A807FF9D3DC70687D2EEB63777A5B (void);
// 0x0000007F System.Single GameSystem::GetFinalTime()
extern void GameSystem_GetFinalTime_m7A4960132FEC8612BB825A08F71D88F1513585B4 (void);
// 0x00000080 System.Void GameSystem::TargetDestroyed(System.Int32)
extern void GameSystem_TargetDestroyed_m2712EA55F852CA734F5C39E2698749247C0AE599 (void);
// 0x00000081 System.Void GameSystem::.ctor()
extern void GameSystem__ctor_mA7E4388F83B0D2F6973EA35D5187A08AC79053B7 (void);
// 0x00000082 System.Void GameSystem::.cctor()
extern void GameSystem__cctor_m41D6AD9FA077788F9A678AD8CFA0BC658600FD0E (void);
// 0x00000083 PoolSystem PoolSystem::get_Instance()
extern void PoolSystem_get_Instance_m3283140B5168DC98E97B9938B1C782E774953EDE (void);
// 0x00000084 System.Void PoolSystem::set_Instance(PoolSystem)
extern void PoolSystem_set_Instance_mEAB4A2532BF747B58AB63768A3C51F9B41DE0F2E (void);
// 0x00000085 System.Void PoolSystem::Create()
extern void PoolSystem_Create_m7A6FA726F04728E2D1854AB76513C335B5EA0C9A (void);
// 0x00000086 System.Void PoolSystem::InitPool(UnityEngine.Object,System.Int32)
extern void PoolSystem_InitPool_m3F9024BF4EE67308BE000ED9E1CD68E24151137A (void);
// 0x00000087 T PoolSystem::GetInstance(UnityEngine.Object)
// 0x00000088 System.Void PoolSystem::SetActive(UnityEngine.Object,System.Boolean)
extern void PoolSystem_SetActive_mBB3CED27ADE1FC60ACFF9E4B332131F36611EC0A (void);
// 0x00000089 System.Void PoolSystem::.ctor()
extern void PoolSystem__ctor_mDDB16443EAA5FB3143FA2B68CC59223DE5CF149B (void);
// 0x0000008A System.Void Projectile::Awake()
extern void Projectile_Awake_m12ADD9C3E81141584C8879C9ECB35F6C89E9FEB2 (void);
// 0x0000008B System.Void Projectile::Launch(Weapon,UnityEngine.Vector3,System.Single)
extern void Projectile_Launch_m2AB4506A0D9AF1D211F963BBD44857B707698B43 (void);
// 0x0000008C System.Void Projectile::OnCollisionEnter(UnityEngine.Collision)
extern void Projectile_OnCollisionEnter_m0EC06F5B4C6ABDB164EC06DD8F010D303CEA9422 (void);
// 0x0000008D System.Void Projectile::Destroy()
extern void Projectile_Destroy_mB6D3179D40B6227FC85332938B0DECF769548C0E (void);
// 0x0000008E System.Void Projectile::Update()
extern void Projectile_Update_m6E056CDE2DC25EDBA5DA3F4D9B9B9A69EC656737 (void);
// 0x0000008F System.Void Projectile::OnDrawGizmosSelected()
extern void Projectile_OnDrawGizmosSelected_m7BB69A9858212CC8878B56AA5DDAF776224648CE (void);
// 0x00000090 System.Void Projectile::.ctor()
extern void Projectile__ctor_m9CB69FCF9C97712F4578D9686861E9694A270A58 (void);
// 0x00000091 System.Void Projectile::.cctor()
extern void Projectile__cctor_m73C7093121D7155A1D11105485464B1602E8C610 (void);
// 0x00000092 System.Void StartCheckpoint::OnTriggerEnter(UnityEngine.Collider)
extern void StartCheckpoint_OnTriggerEnter_m279AD0C58EDCBD66391CD1F71C9FFED7BF36F4C4 (void);
// 0x00000093 System.Void StartCheckpoint::.ctor()
extern void StartCheckpoint__ctor_m6AFD749B52571BD671B9A9BA24A71D4E11D079A4 (void);
// 0x00000094 System.Boolean Target::get_Destroyed()
extern void Target_get_Destroyed_m899997E156C4503AA7AFFDE010661C026A322156 (void);
// 0x00000095 System.Void Target::Awake()
extern void Target_Awake_m5C2BAED793C3F80DB421030B6A3BD26B0C5517C2 (void);
// 0x00000096 System.Void Target::Start()
extern void Target_Start_m16382B4F9DED70B5E55EC0FC7854F97E4FEFE6A5 (void);
// 0x00000097 System.Void Target::Got(System.Single)
extern void Target_Got_mC1235B2A30792510160A353ED66CE463450D2660 (void);
// 0x00000098 System.Void Target::.ctor()
extern void Target__ctor_m5524C5EEF36FA6DB1E6B5BFA323B37E68635C216 (void);
// 0x00000099 System.Void TargetSpawner::Awake()
extern void TargetSpawner_Awake_m2B612A85D1CA647B944883CBF28222E4BC6DEFD6 (void);
// 0x0000009A System.Void TargetSpawner::Dequeue()
extern void TargetSpawner_Dequeue_m8A7AFA88F6312725E812251B45C65C159FC61D75 (void);
// 0x0000009B System.Void TargetSpawner::Update()
extern void TargetSpawner_Update_m35B0381990A4C9394570D17460EEA4A814B97702 (void);
// 0x0000009C System.Void TargetSpawner::.ctor()
extern void TargetSpawner__ctor_mA9EC027F050526ABFE499761DB21BBC6EA25A46B (void);
// 0x0000009D System.Void TargetSpawner/SpawnEvent::.ctor()
extern void SpawnEvent__ctor_m86725DE90CB3F06641579AF55C42981A3E2B7A8C (void);
// 0x0000009E System.Void TargetSpawner/SpawnQueueElement::.ctor()
extern void SpawnQueueElement__ctor_m038BB74C61662441ABDD3744DDEA80C04E07D518 (void);
// 0x0000009F System.Boolean Weapon::get_triggerDown()
extern void Weapon_get_triggerDown_mD60A50F718AC94985A36E36D23CDE050B6D4A6A2 (void);
// 0x000000A0 System.Void Weapon::set_triggerDown(System.Boolean)
extern void Weapon_set_triggerDown_mB18E80FF102FC0A2D84F791273F4EB2AA9230E52 (void);
// 0x000000A1 Weapon/WeaponState Weapon::get_CurrentState()
extern void Weapon_get_CurrentState_mFC52CD3C5F80F0E22B20DBD30EF9ADDC6238A0C8 (void);
// 0x000000A2 System.Int32 Weapon::get_ClipContent()
extern void Weapon_get_ClipContent_mA82F7426117DCBACC13EF43D51C10009CEC04373 (void);
// 0x000000A3 Controller Weapon::get_Owner()
extern void Weapon_get_Owner_m4BB7806EBE7938099BC73AB4515B628AABB66E2D (void);
// 0x000000A4 System.Void Weapon::Awake()
extern void Weapon_Awake_m25895D001658578F1B4557C97E1D2C4E81127B69 (void);
// 0x000000A5 System.Void Weapon::PickedUp(Controller)
extern void Weapon_PickedUp_m7727C0E3A641D2F16D4942D6610558B4F6366BBF (void);
// 0x000000A6 System.Void Weapon::PutAway()
extern void Weapon_PutAway_m49C72179C164395A0841574369B3F942A5046AF2 (void);
// 0x000000A7 System.Void Weapon::Selected()
extern void Weapon_Selected_m53617F3666871FB8F8DB96D5E7130A28B3367AFE (void);
// 0x000000A8 System.Void Weapon::Fire()
extern void Weapon_Fire_m0991C13F9C9A395330E5F600C1B36AF91EA01AFE (void);
// 0x000000A9 System.Void Weapon::RaycastShot()
extern void Weapon_RaycastShot_m961B85D92925B19951A33DFC2E3331ED3F160BC3 (void);
// 0x000000AA System.Void Weapon::ProjectileShot()
extern void Weapon_ProjectileShot_mAF4FCFB07DA7A83A95281F163C70324938066D1D (void);
// 0x000000AB System.Void Weapon::ReturnProjecticle(Projectile)
extern void Weapon_ReturnProjecticle_m74F6E20F4FDB8C30947CA755F1548CC13B75A217 (void);
// 0x000000AC System.Void Weapon::Reload()
extern void Weapon_Reload_m8EC4D50E0E4A338C217530888480D0FAA6EE1A18 (void);
// 0x000000AD System.Void Weapon::Update()
extern void Weapon_Update_m57E854EC3FC8CCF8FC7CB2D509855C8B025413A2 (void);
// 0x000000AE System.Void Weapon::UpdateControllerState()
extern void Weapon_UpdateControllerState_m9B1063191270B73E7003A437C2BBC28C2765A3CE (void);
// 0x000000AF UnityEngine.Vector3 Weapon::GetCorrectedMuzzlePlace()
extern void Weapon_GetCorrectedMuzzlePlace_mA5C15F011F00E3F210DD791B1B6EAEE77CD2AD96 (void);
// 0x000000B0 System.Void Weapon::.ctor()
extern void Weapon__ctor_m6F053F0444AE8DF68EDA99C92CF944903C784332 (void);
// 0x000000B1 System.Void Weapon::.cctor()
extern void Weapon__cctor_mFD9ECD367B20FFAC9411A8F7F8202EDFFFBDAA70 (void);
// 0x000000B2 System.Void Weapon/AdvancedSettings::.ctor()
extern void AdvancedSettings__ctor_m967433C5940F7C1272D64C950EB8A8F66CD12E1B (void);
// 0x000000B3 System.Void Weapon/ActiveTrail::.ctor()
extern void ActiveTrail__ctor_m5B81ADA4444FDB736AB9A037B9946A4DB8709FA7 (void);
// 0x000000B4 System.Void AmmoTypeAttribute::.ctor()
extern void AmmoTypeAttribute__ctor_m343474E2FFA27BE799B736C0E9D6C6E49E4805C1 (void);
// 0x000000B5 System.Void AmmoDisplay::UpdateAmount(System.Int32,System.Int32)
// 0x000000B6 System.Void AmmoDisplay::.ctor()
extern void AmmoDisplay__ctor_m10DE056333E2527414D5F1F93AEB2D037D5FCFCC (void);
// 0x000000B7 System.Void WeaponAnimationEventHandler::Awake()
extern void WeaponAnimationEventHandler_Awake_m0446D16348CA2810B4104B273828CD5EBBD69D42 (void);
// 0x000000B8 System.Void WeaponAnimationEventHandler::PlayFootstep()
extern void WeaponAnimationEventHandler_PlayFootstep_mFEA23CB94368E5981A57E06C00E68ECBC528AFE0 (void);
// 0x000000B9 System.Void WeaponAnimationEventHandler::.ctor()
extern void WeaponAnimationEventHandler__ctor_mABDD4A4AE30D15ED500E0C88373B6A91F5ACAC61 (void);
// 0x000000BA System.Void DissolveAction::Start()
extern void DissolveAction_Start_mA5125B89F482099E5FA22206411C4D3E8AF5D23D (void);
// 0x000000BB System.Void DissolveAction::Update()
extern void DissolveAction_Update_mCD4B13692C75B0E8964EF61BB6683849284B8F34 (void);
// 0x000000BC System.Void DissolveAction::Activated()
extern void DissolveAction_Activated_m4F911855DDBE38EDBA572F1D8125A91F84C4C30B (void);
// 0x000000BD System.Void DissolveAction::.ctor()
extern void DissolveAction__ctor_m8A5FF2126F8D5C7B46B881BB415534F4A89EA535 (void);
// 0x000000BE System.Void EnableObjectAction::Activated()
extern void EnableObjectAction_Activated_m22CC05D0996B6832AA1EDD522DC868CE581A32BC (void);
// 0x000000BF System.Void EnableObjectAction::.ctor()
extern void EnableObjectAction__ctor_m7A616B8AE5E5849BBFB522084DC51E6A6982D9F2 (void);
// 0x000000C0 System.Void GameAction::Activated()
// 0x000000C1 System.Void GameAction::.ctor()
extern void GameAction__ctor_mD38ED919D06973B943C07CEDDEDAED1888BDAA42 (void);
// 0x000000C2 System.Void GameTrigger::Trigger()
extern void GameTrigger_Trigger_m911374A60FEF49E29011CADEE4E7A425B588F111 (void);
// 0x000000C3 System.Void GameTrigger::.ctor()
extern void GameTrigger__ctor_m061F973F35BAFC8303384E425BD26E784942C0FB (void);
// 0x000000C4 System.Void MovingAction::Activated()
extern void MovingAction_Activated_mDC2EA9634EC09B6948B20F6675289844ECA683D8 (void);
// 0x000000C5 System.Void MovingAction::Start()
extern void MovingAction_Start_mD8238C1BE275E90E579477FAC24A64F0F0613342 (void);
// 0x000000C6 System.Void MovingAction::Update()
extern void MovingAction_Update_m9598AF91CB609967D75DCBD255A3D58F526F8958 (void);
// 0x000000C7 System.Void MovingAction::.ctor()
extern void MovingAction__ctor_mCF4DE932D6803AFBE6715CB6299C8087F9E7DB33 (void);
// 0x000000C8 System.Void TargetChecker::Update()
extern void TargetChecker_Update_m6AA8918B89BBB35AA1C73528EFC9FB0EAF063738 (void);
// 0x000000C9 System.Void TargetChecker::.ctor()
extern void TargetChecker__ctor_mF6BB4EC2014195368E56C5A89EA9499A54685AF8 (void);
// 0x000000CA FinalScoreUI FinalScoreUI::get_Instance()
extern void FinalScoreUI_get_Instance_mF304E739F1B1D78D67454212DDACC12A9A414AF2 (void);
// 0x000000CB System.Void FinalScoreUI::set_Instance(FinalScoreUI)
extern void FinalScoreUI_set_Instance_mFA4DB79D3DB33AC667B2537173D8DAE3AEB2D904 (void);
// 0x000000CC System.Void FinalScoreUI::Awake()
extern void FinalScoreUI_Awake_m1AB63E7B2D115CE7B5A3338CDE4DD6499EB515D3 (void);
// 0x000000CD System.Void FinalScoreUI::Display()
extern void FinalScoreUI_Display_mA42A9C9E3977D13322730166FFE952AB18F90A4C (void);
// 0x000000CE System.Void FinalScoreUI::.ctor()
extern void FinalScoreUI__ctor_m0EDC340CF9C42DE96E9527EC2D1BFFB73F15F4D3 (void);
// 0x000000CF FullscreenMap FullscreenMap::get_Instance()
extern void FullscreenMap_get_Instance_mF468407F9D1395A77744A6DD8FA22D9E9ADCFEA2 (void);
// 0x000000D0 System.Void FullscreenMap::set_Instance(FullscreenMap)
extern void FullscreenMap_set_Instance_m7BA8B7B15464513FEE6E2F428E6A44744DE3478D (void);
// 0x000000D1 UnityEngine.RenderTexture FullscreenMap::get_RenderTexture()
extern void FullscreenMap_get_RenderTexture_m25FC94CD8CA86C58B9A760E985E86A74B56D6E2A (void);
// 0x000000D2 System.Void FullscreenMap::Awake()
extern void FullscreenMap_Awake_mECC9C8C0531CDEDEDA06C72A3760F0CC0BB4450D (void);
// 0x000000D3 System.Void FullscreenMap::UpdateForPlayerTransform(UnityEngine.Transform)
extern void FullscreenMap_UpdateForPlayerTransform_m43812D84366F723AB290992521D361FF47ADC9B9 (void);
// 0x000000D4 System.Void FullscreenMap::.ctor()
extern void FullscreenMap__ctor_m874E9D8A178788507E74CFD01DFB8EBB4F016257 (void);
// 0x000000D5 GameSystemInfo GameSystemInfo::get_Instance()
extern void GameSystemInfo_get_Instance_m7D2DAA0390D4628D13D1EF8F794EA7844BF4BFB1 (void);
// 0x000000D6 System.Void GameSystemInfo::set_Instance(GameSystemInfo)
extern void GameSystemInfo_set_Instance_m1AF0A657C13664BF1993734007C48E96AF775708 (void);
// 0x000000D7 System.Void GameSystemInfo::Awake()
extern void GameSystemInfo_Awake_m6B6AAA5FA8F7DCA488A2CABCE8AD6CE7D19CEE1E (void);
// 0x000000D8 System.Void GameSystemInfo::UpdateTimer(System.Single)
extern void GameSystemInfo_UpdateTimer_m52E5650C32C2F835B989396E9F76F67DC8B5E16F (void);
// 0x000000D9 System.Void GameSystemInfo::UpdateScore(System.Int32)
extern void GameSystemInfo_UpdateScore_m79A2D60374FF37F13C4724189923FCEE6A80D452 (void);
// 0x000000DA System.Void GameSystemInfo::.ctor()
extern void GameSystemInfo__ctor_m1A72B0EC8458152A472755A7C5E501EC2EDA2066 (void);
// 0x000000DB LevelSelectionUI LevelSelectionUI::get_Instance()
extern void LevelSelectionUI_get_Instance_mEA569E5612555EA33FA8A7974966A1A0B24EE621 (void);
// 0x000000DC System.Void LevelSelectionUI::set_Instance(LevelSelectionUI)
extern void LevelSelectionUI_set_Instance_m8342C4331F8B5448B7380F08B5552357D9BBE567 (void);
// 0x000000DD System.Void LevelSelectionUI::Awake()
extern void LevelSelectionUI_Awake_m8566BEC69EEC073896E9750328EA56828C8E2B44 (void);
// 0x000000DE System.Void LevelSelectionUI::Init()
extern void LevelSelectionUI_Init_m60EF72401B20B4C71BFF819C1469A69BFD4AA0B0 (void);
// 0x000000DF System.Boolean LevelSelectionUI::IsEmpty()
extern void LevelSelectionUI_IsEmpty_m23848E61F3E236B6848689C79C392E4FFF4A395D (void);
// 0x000000E0 System.Void LevelSelectionUI::BackToPause()
extern void LevelSelectionUI_BackToPause_mDDF9FD7C1D679DD51A72400EDB48F366E31B2C01 (void);
// 0x000000E1 System.Void LevelSelectionUI::BackToEpisode()
extern void LevelSelectionUI_BackToEpisode_m6C1FB59684C7F8F134EEE886F45987B27622A235 (void);
// 0x000000E2 System.Void LevelSelectionUI::DisplayEpisode()
extern void LevelSelectionUI_DisplayEpisode_mE8939BF52A752264DAEFB935D2FC8FB77DCA8081 (void);
// 0x000000E3 System.Void LevelSelectionUI::OpenEpisode(System.Int32)
extern void LevelSelectionUI_OpenEpisode_mD8B547A3AB8524A278724C4E2F2EF063736DA6FF (void);
// 0x000000E4 System.Void LevelSelectionUI::.ctor()
extern void LevelSelectionUI__ctor_mE11496213ABC890F1636B16C157EB6544C085D32 (void);
// 0x000000E5 System.Void LevelSelectionUI/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m78DA9BC04F87E60E513911BE2AB5F3D2DCA35DD1 (void);
// 0x000000E6 System.Void LevelSelectionUI/<>c__DisplayClass10_0::<Init>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CInitU3Eb__0_m197EE432C0E2C60842099FC5FCF26103E2246DC0 (void);
// 0x000000E7 System.Void LevelSelectionUI/<>c__DisplayClass10_1::.ctor()
extern void U3CU3Ec__DisplayClass10_1__ctor_m95B1E9B5C06203338684182D04B608EACDD50168 (void);
// 0x000000E8 System.Void LevelSelectionUI/<>c__DisplayClass10_1::<Init>b__1()
extern void U3CU3Ec__DisplayClass10_1_U3CInitU3Eb__1_m14F7C8EF39A1C3952BD108D3D428B1C1C5528C1F (void);
// 0x000000E9 MinimapUI MinimapUI::get_Instance()
extern void MinimapUI_get_Instance_m1F6767A6CA7FEF075F4AE3816CCEE954FC00CBFD (void);
// 0x000000EA System.Void MinimapUI::set_Instance(MinimapUI)
extern void MinimapUI_set_Instance_m8CB5EA9E3DF061B2656AA94F94F19FA27D28290C (void);
// 0x000000EB UnityEngine.RenderTexture MinimapUI::get_RenderTexture()
extern void MinimapUI_get_RenderTexture_m44C0DDF374EA08D4E36B72BC1E4E8F85725B743B (void);
// 0x000000EC System.Void MinimapUI::Awake()
extern void MinimapUI_Awake_m6E774268648AC486B6EE0E00FF55D7264DF4FFC4 (void);
// 0x000000ED System.Void MinimapUI::Start()
extern void MinimapUI_Start_m9312B674D0B1B905E975BBE84F5102A6B76E504D (void);
// 0x000000EE System.Void MinimapUI::UpdateForPlayerTransform(UnityEngine.Transform)
extern void MinimapUI_UpdateForPlayerTransform_m6791850E5FD7482CB753720724B7DCEA00006E0D (void);
// 0x000000EF System.Void MinimapUI::.ctor()
extern void MinimapUI__ctor_m2F4C8A1A5E3E8D66AE5658C05687CCECC993AE77 (void);
// 0x000000F0 PauseMenu PauseMenu::get_Instance()
extern void PauseMenu_get_Instance_mBDED08FD5E9DA3617F9CDF797EF08F029837794A (void);
// 0x000000F1 System.Void PauseMenu::set_Instance(PauseMenu)
extern void PauseMenu_set_Instance_mD894A36E984B83D12AB80D48789B0FD21B338087 (void);
// 0x000000F2 System.Void PauseMenu::Awake()
extern void PauseMenu_Awake_mD17C2664126E61F76112D3C96B5A4684E0D00B66 (void);
// 0x000000F3 System.Void PauseMenu::Display()
extern void PauseMenu_Display_m3C843FA9EAED4BFD250C9F526D4304C4144F2EC4 (void);
// 0x000000F4 System.Void PauseMenu::OpenEpisode()
extern void PauseMenu_OpenEpisode_mCDD192887DBFD4763EDBA77F9BFA8C99D768630F (void);
// 0x000000F5 System.Void PauseMenu::ReturnToGame()
extern void PauseMenu_ReturnToGame_mC2D805723AFA9151EB761C614D959EA711ACB188 (void);
// 0x000000F6 System.Void PauseMenu::ExitGame()
extern void PauseMenu_ExitGame_m7C23760AD4988B9EC0BBC9D130CD765227B4D574 (void);
// 0x000000F7 System.Void PauseMenu::.ctor()
extern void PauseMenu__ctor_m81B0E020DC5008DA4D414200BAAF7122B430D826 (void);
// 0x000000F8 System.Void RestartLevel::Restart()
extern void RestartLevel_Restart_m87A92D8A4B6AF025EAD0900DF6E64657EC22B1E7 (void);
// 0x000000F9 System.Void RestartLevel::NextLevel()
extern void RestartLevel_NextLevel_m557A9721914185A3F6DD83B786E6A8C93D6218DB (void);
// 0x000000FA System.Void RestartLevel::.ctor()
extern void RestartLevel__ctor_mC621AAD14E20CDCC26868742FEC1F9B84E579618 (void);
// 0x000000FB WeaponInfoUI WeaponInfoUI::get_Instance()
extern void WeaponInfoUI_get_Instance_mFE719CE0468D4030B5E115FE868C71CC08A581F3 (void);
// 0x000000FC System.Void WeaponInfoUI::set_Instance(WeaponInfoUI)
extern void WeaponInfoUI_set_Instance_m36D3B1780E3A413C1E12BA0AE07E9E7B978F99FD (void);
// 0x000000FD System.Void WeaponInfoUI::OnEnable()
extern void WeaponInfoUI_OnEnable_mF79316B079F30E6610EEE886D88997FFC656C5C2 (void);
// 0x000000FE System.Void WeaponInfoUI::UpdateWeaponName(Weapon)
extern void WeaponInfoUI_UpdateWeaponName_m82B4295BE5EB9BCE471408B4D6C21154859DD96D (void);
// 0x000000FF System.Void WeaponInfoUI::UpdateClipInfo(Weapon)
extern void WeaponInfoUI_UpdateClipInfo_m33176395667405FA407349422570B8A5D9BFFD75 (void);
// 0x00000100 System.Void WeaponInfoUI::UpdateAmmoAmount(System.Int32)
extern void WeaponInfoUI_UpdateAmmoAmount_mF958707CD8AF2B7379C5373DCB13648F53AC89C0 (void);
// 0x00000101 System.Void WeaponInfoUI::.ctor()
extern void WeaponInfoUI__ctor_m9C2C5FB141EFB49969AA137C05F69D9A73C28F30 (void);
// 0x00000102 System.Void AnimContr::Start()
extern void AnimContr_Start_m3EA8B03EA816981F2DDD2507C49A7E2F221FA38F (void);
// 0x00000103 System.Void AnimContr::Update()
extern void AnimContr_Update_mFBC76E2D866849186B6B15E22C14D125E6F36E58 (void);
// 0x00000104 System.Void AnimContr::.ctor()
extern void AnimContr__ctor_m55D8DC36390125046A5FE0DBDB49E12479F74D3F (void);
// 0x00000105 System.Void Readme::.ctor()
extern void Readme__ctor_m69C325C4C171DCB0312B646A9034AA91EA8C39C6 (void);
// 0x00000106 System.Void Readme/Section::.ctor()
extern void Section__ctor_m5F732533E4DFC0167D965E5F5DB332E46055399B (void);
static Il2CppMethodPointer s_methodPointers[262] = 
{
	RandomPlayer_get_source_m255EF513B1E0CC9D744C4E7D2F7EC2ACE8EDE686,
	RandomPlayer_Awake_m20C900D30753BC37A250A9914F2C0F58EB8DC0CD,
	RandomPlayer_GetRandomClip_m097681BEF8B2D7685254C499D15FC4CBE9DA336A,
	RandomPlayer_PlayRandom_m46B2095625A2D86225BC1A9E35BF5F31DA2D903B,
	RandomPlayer_PlayClip_mB797975091AF55EC23545DEF90C2EF446261ECE5,
	RandomPlayer__ctor_m75DAC2A102EDA3B588BB016226795206680637E7,
	UIAudioPlayer_get_Instance_mEFA2C5F53C86DAAD2E3F3A87DF3473E0D4CFAC05,
	UIAudioPlayer_set_Instance_m17CD95BE5758E9E62930B0D8DCF13328DCB6922C,
	UIAudioPlayer_Awake_m94912E521D386330DA83A7E2320C730F6D92A292,
	UIAudioPlayer_PlayPositive_mFD7EC92A738D86664BAC4933C73B5DD5BB7DDBF4,
	UIAudioPlayer_PlayNegative_m8EC01980081EA09B5D677D8530AAA276301FA4B6,
	UIAudioPlayer__ctor_m392A9F7A93243476B1A554D9E473A53CDF53ADC6,
	WorldAudioPool_Awake_mD0AA692A8A922BA89881F0913D72C2D5258A3BAF,
	WorldAudioPool_Init_m4C4DBEE92DCF2C61628840D3025DEB2D625DAB39,
	WorldAudioPool_GetWorldSFXSource_m2C4BF9380E7C454AC09787B2D698DAAC8394A148,
	WorldAudioPool__ctor_mB731A9349B3830C2BDAB5455D55A629BC22C2504,
	CameraShaker_get_Instance_m9AAD23D1C29F9766C20354D474EC939D0281730D,
	CameraShaker_set_Instance_m71F7E168A9EB1D5ADA275D7F61D7FCA4AF18CC43,
	CameraShaker_Awake_m7BF082AA73D6159B01E22FE07935670BAC0545EA,
	CameraShaker_Update_m0168605AE22B1672350A6A94966649A653F00655,
	CameraShaker_Shake_m22A0C6974F2877F9BE6B6555C1A8775E92D7144F,
	CameraShaker__ctor_m302CB9C4B3D79B3D8E67A68E91F9383348B44891,
	ImpactManager_get_Instance_m7981ADB40706C439E5B984E94ACEB5C8C1509FD1,
	ImpactManager_set_Instance_m2C8165FDAEA813F2798CF85640609DCCBF8024A3,
	ImpactManager_Awake_mFAA328BB40CFE935BF7D6276D524E71A6DEE4745,
	ImpactManager_Start_mF0F6F179FADD52A235D5031AFB3CA145BB05AFFE,
	ImpactManager_PlayImpact_mB8A8290E212FCA8D5FF2DD5E0A2DBB15AD1317EC,
	ImpactManager__ctor_m1C000330332AD9116F08AD5353F4F1A28AA4DBB5,
	ImpactSetting__ctor_mA704E5F4FEBC43B7B0FDA460C1BF16771845306C,
	LiquidAmmoDisplay_UpdateAmount_m278B68AAB87187869E82B0955454EC9525DEB9F5,
	LiquidAmmoDisplay__ctor_m7ABD5576B1FE1008589D22FE2DC2D2CD33D94AB4,
	LiquidContainer_Awake_m4AB75F7622714E43081C32003E2E9716458F7BD0,
	LiquidContainer_ChangeLiquidAmount_mBCCB8CF030FA0FBE2225024725BFDDDB3B5020BF,
	LiquidContainer_Update_mAE5BB50E6E162022E7316582A0E9C68D65C48862,
	LiquidContainer__ctor_m8A09F5F079F9878D05964C398BB1A6636B9AD182,
	TextAmmoDisplay_UpdateAmount_mA625E78DBD77EE1DE128A24F6ED630B641329820,
	TextAmmoDisplay__ctor_mE3310C49F1AFA8885169A498AC989E90BA068387,
	GameDatabase_get_Instance_mE05AC3BD084D8B3F9C434530B8BCC54AC3A8218E,
	GameDatabase__ctor_mC94FDE78AC36519F4B5A706B0C6740EC4763C9EB,
	AmmoDatabase_GetEntry_m6DF800F060D1F1899EB26149E23C952A8FCAC95B,
	AmmoDatabase_GetEntry_m6C5E57B313250C9198FF3385827FE9DDE5903E36,
	AmmoDatabase__ctor_m53114069A022D9B731946CBD88450D6954B755EC,
	Entry__ctor_m1C2E8822676326F7DF6F491C32D14331B7C26AB7,
	U3CU3Ec__DisplayClass4_0__ctor_mB78831EE7C0FE427AA7D303094FCA0E4833DBCEF,
	U3CU3Ec__DisplayClass4_0_U3CGetEntryU3Eb__0_mB16C0D70A20D9F9C0A2ECE752B1839A0BE5495C7,
	U3CU3Ec__DisplayClass5_0__ctor_mA5AC537E51982A1752621DF0F531C5BA5D60C977,
	U3CU3Ec__DisplayClass5_0_U3CGetEntryU3Eb__0_m78B9ED31A1276B5FFC579ABBCFB10DC3D5000538,
	Episode__ctor_m20423B23EB6A8A2EB71D2B4F9001493599498BCE,
	GradientTexture__ctor_m6E78726316E8C19C00BF643E91EE03E0D0675DAD,
	PathSystem_get_worldNode_m283EC603B5E180BD357F4A134262704A608B7E74,
	PathSystem_Init_m815E1596DDA5EAE471ED7706038313410128DEE6,
	PathSystem_InitData_m026DB59D4E9B270B7AD581806819958833E73777,
	PathSystem_Move_m73D5CBEA14BDAEE4033F24D6B4BA4F9C754F0EA9,
	PathSystem__ctor_m419DE0D77AD1F951409F2A0C3EA1ECCCE67E21EB,
	PathData__ctor_mA45CBE4566016FE84EF18EE1BC93FF4EAE479788,
	Helpers_RecursiveLayerChange_m845740834A96063756F1B5534763F1537FC8E76A,
	Helpers__ctor_mE14820E4C735448B10388645E26A1EE606E86A8B,
	Key_OnEnable_m816D2FC38A3D7C9F5D8AF910D7183245633C8ECF,
	Key_OnTriggerEnter_m9DB36FB68944E6C173FF7CE7F1393676785FE813,
	Key__ctor_mA069FB77FE5BAEC833A31356AB0867315F988954,
	Keychain_GrabbedKey_mCE72B9E93D32E9BEBC80F38D716A22FBBBD9A8B0,
	Keychain_HaveKey_m920DCA3580AC90AE8030019CB7634180E9421852,
	Keychain_UseKey_mA7DAEFBB9A40F590FF322A56F2BC482852A5B493,
	Keychain__ctor_mFE5336B1256A28FAAAD1F4AEA5270A14532FC667,
	Lock_Start_mBA33705016890B08069EBE6F715EB8453E5C47B2,
	Lock_OnTriggerEnter_mCD982438199FD138CC2F8323F221D36ACBBF267C,
	Lock_OnTriggerExit_m6CFA56B36A66C401EE6F3C59D2C786D284AB571C,
	Lock_Opened_mD57A8F76630F23AC79799119FF5C3D2C92EC03BB,
	Lock__ctor_m11B2038D869328BCF986F07428B6FCC7EE8DEFFD,
	LevelLayout_get_Destroyed_mC27334F207CE70D82A801B13B1726B3B73070C6E,
	LevelLayout_set_Destroyed_mB32F773825499ECDC402265D38DC01299BF0FEED,
	LevelLayout_OnDestroy_m8702D1C5D6A9F863AD7327EB31430194E36C00EE,
	LevelLayout__ctor_m2DCC4F7A20D2FCFA0E3ECDB1019D734893D5DDD6,
	LevelRoom_Placed_m335DC1E8BC6A7F311C10FD2F46C47EB471518E21,
	LevelRoom__ctor_m3AC6CC6E6509946452170907A85EB773A87D3F93,
	LevelRoomGroup__ctor_m1189DE70C404AC53303F2D76EAA0615A7F9EC542,
	MinimapElement_get_Renderers_m5B5F9479EC9F627A1772D40FC0FB89B153C726F2,
	MinimapElement_OnEnable_m53672F3A01196CAF9583F33EBCF185F86039112B,
	MinimapElement_OnDisable_m9716243A9306FB34C4BB92888B4DCBB4BB349DEE,
	MinimapElement__ctor_mA06D434E62FA5B2D9666C5C5C5CE2E4CB0439B10,
	MinimapElement__cctor_mF8C9598AA87CCC02FF8B5691815F720443D1FBCA,
	MinimapSystem__cctor_mE9C922A33BD055943F22BB1851A079275A213497,
	MinimapSystem_Render_mA320EBFF70876AE845D7C45ECDED95A80B1BA429,
	MinimapSystem__ctor_mD80DC40AE6D7C856B60AB86A013192E7B96AD708,
	AmmoBox_Reset_mE0F24551744E709F3442287ED3442BFED9E38C5E,
	AmmoBox_OnTriggerEnter_mCAB57357B5EEAB210C70A4AF9094EAEA8D21D1A9,
	AmmoBox__ctor_m5041D6AC701B7BD9B7BE340E12DB8A8330686F6E,
	AmmoInventoryEntry__ctor_mE1EB635B5375A539C7B0B2F8A7E352FF1001C7F4,
	Controller_get_Instance_m4C033AEA0A4452523EC3948C1505B1417BC985B0,
	Controller_set_Instance_m88C95BE455936719549C754DA7C5486B9C3A1090,
	Controller_get_Speed_mCE7CC38AC975379497269D1E0AC5BF0855987859,
	Controller_set_Speed_mF91E05548BA70CEB77E4AB4352245DFA9E06DCCF,
	Controller_get_LockControl_m1B590B98AD884F7DA775A9522974715005E1882D,
	Controller_set_LockControl_m76FC9E9F865EE9A8DD003EBDF074C40115990BC2,
	Controller_get_CanPause_m1500FCAA6CA05B5916C48EEC0A0689773EE32803,
	Controller_set_CanPause_mE5F4347A8E644CFCD5CFEDBDA3C9D47E9F1A1653,
	Controller_get_Grounded_mAFB6D1C160D6E8A94FA2EEA25BEEE92A00711E91,
	Controller_Awake_mB1780968013F87D703550B691ADB9A9817150FE4,
	Controller_Start_m6CF9E52A1D22548B67650167CBA335928CEC4796,
	Controller_Update_mDE8E185DE18C109FA2E8C6FCBFEB86F100F21634,
	Controller_DisplayCursor_mD863823D42057267BE1E1FF24C7F6437D9F211E6,
	Controller_PickupWeapon_m2457CA969AB446D80FBD5A3C677D79854291ECD0,
	Controller_ChangeWeapon_mB276822F9046AA92CF79AD7EBFC7408C99A15EFA,
	Controller_GetAmmo_mD9E08D9150B1C342114AE03C7511EECAA4C02D2A,
	Controller_ChangeAmmo_mB7A524B0D0EB728D63DF917A262E02F38030464B,
	Controller_PlayFootstep_mC8832354A76947C8D9A10229ADB156A6B5FCB4F2,
	Controller__ctor_mD8A105DFD9CAFDD0B419A5ECD5BC3B789338476A,
	U3CU3Ec__DisplayClass46_0__ctor_m51D8D10A510BEAF6DDCB82727E77F8BE3F91760F,
	U3CU3Ec__DisplayClass46_0_U3CPickupWeaponU3Eb__0_m9A19576F2B459803A02C0C712209DCFDC7283301,
	EndCheckpoint_OnTriggerEnter_mD0E98E9442E6C40B7D9F846AD0862FD6162F2AED,
	EndCheckpoint__ctor_mBCCDDEB1E7AFAD107C9E7474654F110EA90A4390,
	GameSystem_get_Instance_m95EEFCB95AC313F4828A0A5EB13AE8848AB07C07,
	GameSystem_set_Instance_m9184B3938238957EF57D0DDA53045213264C4AC4,
	GameSystem_get_RunTime_m4CA3A03B46332D7FC7FC585DD2D01E89446616B6,
	GameSystem_get_TargetCount_m30571DC7D5F32D51B7EB995B6C534A9C96B92A4D,
	GameSystem_get_DestroyedTarget_mC6760EB74CC37D80D9BCEF6A40CDA874862754FF,
	GameSystem_get_Score_mDAA1F04F61BC57A54C8532EE72DF03EEEB0BD4E3,
	GameSystem_Awake_m49B2786FF71ACC6EAAC99F840AC0F92E18ADB259,
	GameSystem_Start_mF9E1A5B3DDCD3AF572B2C9251405804B47CF6241,
	GameSystem_ResetTimer_mA439459F5468ED84DE39F1B0474ABC964618648E,
	GameSystem_StartTimer_mA8212E89CC3FFE1436C6F12B4F345492352B483F,
	GameSystem_StopTimer_m077F3C5C3F3ABCF19AF0AED8040E99AA6F8DE37B,
	GameSystem_FinishRun_m3C616268ECF405A4AB7D8131EAB8F6492C1BBF7D,
	GameSystem_NextLevel_mBFE5FCA6C9CB0B7B22076103CA25C00D6B4F2A68,
	GameSystem_RetrieveTargetsCount_mC7AD75F15F0465EDF5D215AC4B0823E6B17C2700,
	GameSystem_Update_m2AC59D669C6A807FF9D3DC70687D2EEB63777A5B,
	GameSystem_GetFinalTime_m7A4960132FEC8612BB825A08F71D88F1513585B4,
	GameSystem_TargetDestroyed_m2712EA55F852CA734F5C39E2698749247C0AE599,
	GameSystem__ctor_mA7E4388F83B0D2F6973EA35D5187A08AC79053B7,
	GameSystem__cctor_m41D6AD9FA077788F9A678AD8CFA0BC658600FD0E,
	PoolSystem_get_Instance_m3283140B5168DC98E97B9938B1C782E774953EDE,
	PoolSystem_set_Instance_mEAB4A2532BF747B58AB63768A3C51F9B41DE0F2E,
	PoolSystem_Create_m7A6FA726F04728E2D1854AB76513C335B5EA0C9A,
	PoolSystem_InitPool_m3F9024BF4EE67308BE000ED9E1CD68E24151137A,
	NULL,
	PoolSystem_SetActive_mBB3CED27ADE1FC60ACFF9E4B332131F36611EC0A,
	PoolSystem__ctor_mDDB16443EAA5FB3143FA2B68CC59223DE5CF149B,
	Projectile_Awake_m12ADD9C3E81141584C8879C9ECB35F6C89E9FEB2,
	Projectile_Launch_m2AB4506A0D9AF1D211F963BBD44857B707698B43,
	Projectile_OnCollisionEnter_m0EC06F5B4C6ABDB164EC06DD8F010D303CEA9422,
	Projectile_Destroy_mB6D3179D40B6227FC85332938B0DECF769548C0E,
	Projectile_Update_m6E056CDE2DC25EDBA5DA3F4D9B9B9A69EC656737,
	Projectile_OnDrawGizmosSelected_m7BB69A9858212CC8878B56AA5DDAF776224648CE,
	Projectile__ctor_m9CB69FCF9C97712F4578D9686861E9694A270A58,
	Projectile__cctor_m73C7093121D7155A1D11105485464B1602E8C610,
	StartCheckpoint_OnTriggerEnter_m279AD0C58EDCBD66391CD1F71C9FFED7BF36F4C4,
	StartCheckpoint__ctor_m6AFD749B52571BD671B9A9BA24A71D4E11D079A4,
	Target_get_Destroyed_m899997E156C4503AA7AFFDE010661C026A322156,
	Target_Awake_m5C2BAED793C3F80DB421030B6A3BD26B0C5517C2,
	Target_Start_m16382B4F9DED70B5E55EC0FC7854F97E4FEFE6A5,
	Target_Got_mC1235B2A30792510160A353ED66CE463450D2660,
	Target__ctor_m5524C5EEF36FA6DB1E6B5BFA323B37E68635C216,
	TargetSpawner_Awake_m2B612A85D1CA647B944883CBF28222E4BC6DEFD6,
	TargetSpawner_Dequeue_m8A7AFA88F6312725E812251B45C65C159FC61D75,
	TargetSpawner_Update_m35B0381990A4C9394570D17460EEA4A814B97702,
	TargetSpawner__ctor_mA9EC027F050526ABFE499761DB21BBC6EA25A46B,
	SpawnEvent__ctor_m86725DE90CB3F06641579AF55C42981A3E2B7A8C,
	SpawnQueueElement__ctor_m038BB74C61662441ABDD3744DDEA80C04E07D518,
	Weapon_get_triggerDown_mD60A50F718AC94985A36E36D23CDE050B6D4A6A2,
	Weapon_set_triggerDown_mB18E80FF102FC0A2D84F791273F4EB2AA9230E52,
	Weapon_get_CurrentState_mFC52CD3C5F80F0E22B20DBD30EF9ADDC6238A0C8,
	Weapon_get_ClipContent_mA82F7426117DCBACC13EF43D51C10009CEC04373,
	Weapon_get_Owner_m4BB7806EBE7938099BC73AB4515B628AABB66E2D,
	Weapon_Awake_m25895D001658578F1B4557C97E1D2C4E81127B69,
	Weapon_PickedUp_m7727C0E3A641D2F16D4942D6610558B4F6366BBF,
	Weapon_PutAway_m49C72179C164395A0841574369B3F942A5046AF2,
	Weapon_Selected_m53617F3666871FB8F8DB96D5E7130A28B3367AFE,
	Weapon_Fire_m0991C13F9C9A395330E5F600C1B36AF91EA01AFE,
	Weapon_RaycastShot_m961B85D92925B19951A33DFC2E3331ED3F160BC3,
	Weapon_ProjectileShot_mAF4FCFB07DA7A83A95281F163C70324938066D1D,
	Weapon_ReturnProjecticle_m74F6E20F4FDB8C30947CA755F1548CC13B75A217,
	Weapon_Reload_m8EC4D50E0E4A338C217530888480D0FAA6EE1A18,
	Weapon_Update_m57E854EC3FC8CCF8FC7CB2D509855C8B025413A2,
	Weapon_UpdateControllerState_m9B1063191270B73E7003A437C2BBC28C2765A3CE,
	Weapon_GetCorrectedMuzzlePlace_mA5C15F011F00E3F210DD791B1B6EAEE77CD2AD96,
	Weapon__ctor_m6F053F0444AE8DF68EDA99C92CF944903C784332,
	Weapon__cctor_mFD9ECD367B20FFAC9411A8F7F8202EDFFFBDAA70,
	AdvancedSettings__ctor_m967433C5940F7C1272D64C950EB8A8F66CD12E1B,
	ActiveTrail__ctor_m5B81ADA4444FDB736AB9A037B9946A4DB8709FA7,
	AmmoTypeAttribute__ctor_m343474E2FFA27BE799B736C0E9D6C6E49E4805C1,
	NULL,
	AmmoDisplay__ctor_m10DE056333E2527414D5F1F93AEB2D037D5FCFCC,
	WeaponAnimationEventHandler_Awake_m0446D16348CA2810B4104B273828CD5EBBD69D42,
	WeaponAnimationEventHandler_PlayFootstep_mFEA23CB94368E5981A57E06C00E68ECBC528AFE0,
	WeaponAnimationEventHandler__ctor_mABDD4A4AE30D15ED500E0C88373B6A91F5ACAC61,
	DissolveAction_Start_mA5125B89F482099E5FA22206411C4D3E8AF5D23D,
	DissolveAction_Update_mCD4B13692C75B0E8964EF61BB6683849284B8F34,
	DissolveAction_Activated_m4F911855DDBE38EDBA572F1D8125A91F84C4C30B,
	DissolveAction__ctor_m8A5FF2126F8D5C7B46B881BB415534F4A89EA535,
	EnableObjectAction_Activated_m22CC05D0996B6832AA1EDD522DC868CE581A32BC,
	EnableObjectAction__ctor_m7A616B8AE5E5849BBFB522084DC51E6A6982D9F2,
	NULL,
	GameAction__ctor_mD38ED919D06973B943C07CEDDEDAED1888BDAA42,
	GameTrigger_Trigger_m911374A60FEF49E29011CADEE4E7A425B588F111,
	GameTrigger__ctor_m061F973F35BAFC8303384E425BD26E784942C0FB,
	MovingAction_Activated_mDC2EA9634EC09B6948B20F6675289844ECA683D8,
	MovingAction_Start_mD8238C1BE275E90E579477FAC24A64F0F0613342,
	MovingAction_Update_m9598AF91CB609967D75DCBD255A3D58F526F8958,
	MovingAction__ctor_mCF4DE932D6803AFBE6715CB6299C8087F9E7DB33,
	TargetChecker_Update_m6AA8918B89BBB35AA1C73528EFC9FB0EAF063738,
	TargetChecker__ctor_mF6BB4EC2014195368E56C5A89EA9499A54685AF8,
	FinalScoreUI_get_Instance_mF304E739F1B1D78D67454212DDACC12A9A414AF2,
	FinalScoreUI_set_Instance_mFA4DB79D3DB33AC667B2537173D8DAE3AEB2D904,
	FinalScoreUI_Awake_m1AB63E7B2D115CE7B5A3338CDE4DD6499EB515D3,
	FinalScoreUI_Display_mA42A9C9E3977D13322730166FFE952AB18F90A4C,
	FinalScoreUI__ctor_m0EDC340CF9C42DE96E9527EC2D1BFFB73F15F4D3,
	FullscreenMap_get_Instance_mF468407F9D1395A77744A6DD8FA22D9E9ADCFEA2,
	FullscreenMap_set_Instance_m7BA8B7B15464513FEE6E2F428E6A44744DE3478D,
	FullscreenMap_get_RenderTexture_m25FC94CD8CA86C58B9A760E985E86A74B56D6E2A,
	FullscreenMap_Awake_mECC9C8C0531CDEDEDA06C72A3760F0CC0BB4450D,
	FullscreenMap_UpdateForPlayerTransform_m43812D84366F723AB290992521D361FF47ADC9B9,
	FullscreenMap__ctor_m874E9D8A178788507E74CFD01DFB8EBB4F016257,
	GameSystemInfo_get_Instance_m7D2DAA0390D4628D13D1EF8F794EA7844BF4BFB1,
	GameSystemInfo_set_Instance_m1AF0A657C13664BF1993734007C48E96AF775708,
	GameSystemInfo_Awake_m6B6AAA5FA8F7DCA488A2CABCE8AD6CE7D19CEE1E,
	GameSystemInfo_UpdateTimer_m52E5650C32C2F835B989396E9F76F67DC8B5E16F,
	GameSystemInfo_UpdateScore_m79A2D60374FF37F13C4724189923FCEE6A80D452,
	GameSystemInfo__ctor_m1A72B0EC8458152A472755A7C5E501EC2EDA2066,
	LevelSelectionUI_get_Instance_mEA569E5612555EA33FA8A7974966A1A0B24EE621,
	LevelSelectionUI_set_Instance_m8342C4331F8B5448B7380F08B5552357D9BBE567,
	LevelSelectionUI_Awake_m8566BEC69EEC073896E9750328EA56828C8E2B44,
	LevelSelectionUI_Init_m60EF72401B20B4C71BFF819C1469A69BFD4AA0B0,
	LevelSelectionUI_IsEmpty_m23848E61F3E236B6848689C79C392E4FFF4A395D,
	LevelSelectionUI_BackToPause_mDDF9FD7C1D679DD51A72400EDB48F366E31B2C01,
	LevelSelectionUI_BackToEpisode_m6C1FB59684C7F8F134EEE886F45987B27622A235,
	LevelSelectionUI_DisplayEpisode_mE8939BF52A752264DAEFB935D2FC8FB77DCA8081,
	LevelSelectionUI_OpenEpisode_mD8B547A3AB8524A278724C4E2F2EF063736DA6FF,
	LevelSelectionUI__ctor_mE11496213ABC890F1636B16C157EB6544C085D32,
	U3CU3Ec__DisplayClass10_0__ctor_m78DA9BC04F87E60E513911BE2AB5F3D2DCA35DD1,
	U3CU3Ec__DisplayClass10_0_U3CInitU3Eb__0_m197EE432C0E2C60842099FC5FCF26103E2246DC0,
	U3CU3Ec__DisplayClass10_1__ctor_m95B1E9B5C06203338684182D04B608EACDD50168,
	U3CU3Ec__DisplayClass10_1_U3CInitU3Eb__1_m14F7C8EF39A1C3952BD108D3D428B1C1C5528C1F,
	MinimapUI_get_Instance_m1F6767A6CA7FEF075F4AE3816CCEE954FC00CBFD,
	MinimapUI_set_Instance_m8CB5EA9E3DF061B2656AA94F94F19FA27D28290C,
	MinimapUI_get_RenderTexture_m44C0DDF374EA08D4E36B72BC1E4E8F85725B743B,
	MinimapUI_Awake_m6E774268648AC486B6EE0E00FF55D7264DF4FFC4,
	MinimapUI_Start_m9312B674D0B1B905E975BBE84F5102A6B76E504D,
	MinimapUI_UpdateForPlayerTransform_m6791850E5FD7482CB753720724B7DCEA00006E0D,
	MinimapUI__ctor_m2F4C8A1A5E3E8D66AE5658C05687CCECC993AE77,
	PauseMenu_get_Instance_mBDED08FD5E9DA3617F9CDF797EF08F029837794A,
	PauseMenu_set_Instance_mD894A36E984B83D12AB80D48789B0FD21B338087,
	PauseMenu_Awake_mD17C2664126E61F76112D3C96B5A4684E0D00B66,
	PauseMenu_Display_m3C843FA9EAED4BFD250C9F526D4304C4144F2EC4,
	PauseMenu_OpenEpisode_mCDD192887DBFD4763EDBA77F9BFA8C99D768630F,
	PauseMenu_ReturnToGame_mC2D805723AFA9151EB761C614D959EA711ACB188,
	PauseMenu_ExitGame_m7C23760AD4988B9EC0BBC9D130CD765227B4D574,
	PauseMenu__ctor_m81B0E020DC5008DA4D414200BAAF7122B430D826,
	RestartLevel_Restart_m87A92D8A4B6AF025EAD0900DF6E64657EC22B1E7,
	RestartLevel_NextLevel_m557A9721914185A3F6DD83B786E6A8C93D6218DB,
	RestartLevel__ctor_mC621AAD14E20CDCC26868742FEC1F9B84E579618,
	WeaponInfoUI_get_Instance_mFE719CE0468D4030B5E115FE868C71CC08A581F3,
	WeaponInfoUI_set_Instance_m36D3B1780E3A413C1E12BA0AE07E9E7B978F99FD,
	WeaponInfoUI_OnEnable_mF79316B079F30E6610EEE886D88997FFC656C5C2,
	WeaponInfoUI_UpdateWeaponName_m82B4295BE5EB9BCE471408B4D6C21154859DD96D,
	WeaponInfoUI_UpdateClipInfo_m33176395667405FA407349422570B8A5D9BFFD75,
	WeaponInfoUI_UpdateAmmoAmount_mF958707CD8AF2B7379C5373DCB13648F53AC89C0,
	WeaponInfoUI__ctor_m9C2C5FB141EFB49969AA137C05F69D9A73C28F30,
	AnimContr_Start_m3EA8B03EA816981F2DDD2507C49A7E2F221FA38F,
	AnimContr_Update_mFBC76E2D866849186B6B15E22C14D125E6F36E58,
	AnimContr__ctor_m55D8DC36390125046A5FE0DBDB49E12479F74D3F,
	Readme__ctor_m69C325C4C171DCB0312B646A9034AA91EA8C39C6,
	Section__ctor_m5F732533E4DFC0167D965E5F5DB332E46055399B,
};
static const int32_t s_InvokerIndices[262] = 
{
	4203,
	4300,
	4203,
	4300,
	1056,
	4300,
	6345,
	6267,
	4300,
	6377,
	6377,
	4300,
	4300,
	6377,
	6345,
	4300,
	6345,
	6267,
	4300,
	4300,
	1986,
	4300,
	6345,
	6267,
	4300,
	4300,
	1085,
	4300,
	4300,
	1771,
	4300,
	4300,
	3512,
	4300,
	4300,
	1771,
	4300,
	6345,
	4300,
	3079,
	3076,
	4300,
	4300,
	4300,
	2488,
	4300,
	2488,
	4300,
	4300,
	4203,
	3470,
	3470,
	1371,
	4300,
	4300,
	5837,
	4300,
	4300,
	3470,
	4300,
	3470,
	2488,
	3470,
	4300,
	4300,
	3470,
	3470,
	4300,
	4300,
	4140,
	3409,
	4300,
	4300,
	3470,
	4300,
	4300,
	6345,
	4300,
	4300,
	4300,
	6377,
	6377,
	5091,
	4300,
	4300,
	3470,
	4300,
	4300,
	6345,
	6267,
	4250,
	3512,
	4140,
	3409,
	4140,
	3409,
	4140,
	4300,
	4300,
	4300,
	3409,
	3470,
	3452,
	2897,
	1771,
	4300,
	4300,
	4300,
	2488,
	3470,
	4300,
	6345,
	6267,
	4250,
	4183,
	4183,
	4183,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	4250,
	3452,
	4300,
	6377,
	6345,
	6267,
	6377,
	1942,
	0,
	5836,
	4300,
	4300,
	1060,
	3470,
	4300,
	4300,
	4300,
	4300,
	6377,
	3470,
	4300,
	4140,
	4300,
	4300,
	3512,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	4140,
	3409,
	4183,
	4183,
	4203,
	4300,
	3470,
	4300,
	4300,
	4300,
	4300,
	4300,
	3470,
	4300,
	4300,
	4300,
	4292,
	4300,
	6377,
	4300,
	4300,
	4300,
	0,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	0,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	6345,
	6267,
	4300,
	4300,
	4300,
	6345,
	6267,
	4203,
	4300,
	3470,
	4300,
	6345,
	6267,
	4300,
	3512,
	3452,
	4300,
	6345,
	6267,
	4300,
	4300,
	4140,
	4300,
	4300,
	4300,
	3452,
	4300,
	4300,
	4300,
	4300,
	4300,
	6345,
	6267,
	4203,
	4300,
	4300,
	3470,
	4300,
	6345,
	6267,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
	6345,
	6267,
	4300,
	3470,
	3470,
	3452,
	4300,
	4300,
	4300,
	4300,
	4300,
	4300,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000087, { 0, 1 } },
};
extern const uint32_t g_rgctx_T_tAC50D5AC9A52C481B224E965C3531CBF4EAEFBFF;
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tAC50D5AC9A52C481B224E965C3531CBF4EAEFBFF },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	262,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
