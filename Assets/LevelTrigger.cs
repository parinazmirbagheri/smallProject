using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTrigger : MonoBehaviour
{
    [SerializeField] float healthThreshold;

    private void OnTriggerEnter(Collider other)
    {
        gameObject.GetComponent<BoxCollider>().enabled = false;
        other.gameObject.GetComponent<Controller>().checkHealth(healthThreshold);
        gameObject.GetComponent<BoxCollider>().enabled = false;
        //print("Player entered");

    }
}
