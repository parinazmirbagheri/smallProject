using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{

    //public int maxHP;
    //public int currentHP;
    public Image mask;


    public void GetCurrentFill(float currentHP, float maxHP)
    {
        float fillAmount = currentHP / maxHP;
        mask.fillAmount = fillAmount;
    }
}
