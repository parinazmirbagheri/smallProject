using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarrationMoments : MonoBehaviour
{

    [SerializeField] GameObject subtitle;
    [SerializeField] AudioSource conversation;
    [SerializeField] GameObject companion;

    private void OnTriggerEnter(Collider other)
    {

        if (gameObject.name.Contains("Encouraging") || gameObject.name.Contains("Checkpoint"))
        {

            StartCoroutine(companion.GetComponent<AnimContr>().BoundingMoments(subtitle, conversation));
            gameObject.GetComponent<BoxCollider>().enabled = false;

        }

        else if (PlayerPrefs.GetInt(gameObject.name) == 0)
        {

            StartCoroutine(companion.GetComponent<AnimContr>().BoundingMoments(subtitle, conversation));
            gameObject.GetComponent<BoxCollider>().enabled = false;

            PlayerPrefs.SetInt(gameObject.name, 1);
        }

    }
}
