using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthKitPickup : MonoBehaviour
{

    [SerializeField] int healValue;
    [SerializeField] AudioSource audioSource;
    // Start is called before the first frame update

    private void Update()
    {
        transform.Rotate(new Vector3(0, 50, 0) * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            audioSource.Play();
            StartCoroutine(DesolveHealthKit(other));

        }
    }

    private IEnumerator DesolveHealthKit(Collider other)
    {
        float delay = 0.25f;
        WaitForSeconds wait = new WaitForSeconds(delay);

        Controller c = other.GetComponent<Controller>();

        yield return wait;

        audioSource.Stop();

        if (c != null && !audioSource.isPlaying)
        {
            c.Healed(healValue);
            Destroy(gameObject);
        }
    }
}
