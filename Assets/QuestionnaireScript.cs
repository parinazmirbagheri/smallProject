using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionnaireScript : MonoBehaviour
{

    [SerializeField] GameObject questionnaireUI;
    [SerializeField] GameObject subtitle;
    [SerializeField] AudioSource conversation;
    [SerializeField] GameObject companion;
    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(companion.GetComponent<AnimContr>().BoundingMoments(subtitle, conversation));
        questionnaireUI.SetActive(true);
        gameObject.GetComponent<BoxCollider>().enabled = false;
        print("Entered");
        GameObject.Find("Character").GetComponent<Controller>().DisplayCursor(true);
        //GameObject.Find("QuestionnaireUI").SetActive(true);


    }

    public void Disable()
    {
        questionnaireUI.SetActive(false);
        GameObject.Find("Character").GetComponent<Controller>().DisplayCursor(false);

        //GameObject.Find("QuestionnaireUI").SetActive(false);

    }

}
