using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HealthBarInfoUI : MonoBehaviour
{
    public static HealthBarInfoUI Instance { get; private set; }

    public Text HealthBarAmount;
    [SerializeField] GameObject progressBar;

    void OnEnable()
    {
        Instance = this;
    }

    public void UpdateHealthBarAmount(float amount)
    {
        //HealthBarAmount.text = "HP: " + amount.ToString();
        progressBar.GetComponent<ProgressBar>().GetCurrentFill(amount, 100f);
    }
}
