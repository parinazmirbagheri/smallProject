﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimContr : MonoBehaviour
{
    // Start is called before the first frame update
    public Animator anim;
    [SerializeField] AudioSource introductionAudio;
    [SerializeField] AudioSource enemyInstructionAudio;
    [SerializeField] AudioSource finalInstructionAudio;
    [SerializeField] AudioSource helpInstructionAudio;
    [SerializeField] AudioSource keyDoorInstructionAudio;
    [SerializeField] List<AudioSource> soothAudios;
    [SerializeField] List<AudioSource> randomDialogues;


    [SerializeField] Transform helpBoxPosition;
    [SerializeField] GameObject ammoPrefab;
    [SerializeField] GameObject healthPackPrefab;
    [SerializeField] List<GameObject> subtitlePrefab;
    [SerializeField] List<GameObject> soothSubtitles;
    [SerializeField] List<GameObject> randomDialoguesSubtitles;


    private int levelRestart;
    private float randomDialogeCoolingTime = 60f;


    private void Awake()
    {
        anim = GetComponent<Animator>();
        introductionAudio = GetComponent<AudioSource>();
        DontDestroyOnLoad(gameObject);
        try
        {
            levelRestart = PlayerPrefs.GetInt("isLoaded");
        }
        catch
        {
            levelRestart = 0;
        } 

        if (levelRestart == 0)
        {

            StartCoroutine(Intro());

        }

        else
        {

            StartCoroutine(CompanionSooth());
        }
    }

    private IEnumerator Intro()
    {
        GameObject.Find("Character").GetComponent<Controller>().isMoving = false;
        introductionAudio.PlayDelayed(2.5f);
        yield return new WaitForSeconds(2.5f);
        subtitlePrefab[0].SetActive(true);

    }

    // Update is called once per frame
    void Update()
    {

        if (!introductionAudio.isPlaying || levelRestart > 0)
        {
            anim.SetTrigger("CompanionMode");
            subtitlePrefab[0].SetActive(false);
            GameObject.Find("Character").GetComponent<Controller>().isMoving = true;


        }


        if (Input.GetKeyDown(KeyCode.Q))
        {
            anim.SetBool("isHidden", true);
            subtitlePrefab[3].SetActive(false);

        }

        if (!introductionAudio.isPlaying && !anim.GetBool("isHidden"))
        {

            int rand = UnityEngine.Random.Range(0, 10);

            if (rand == 0 && randomDialogeCoolingTime < 0)
            {

                int randDialogues = UnityEngine.Random.Range(0, 6);
                print(randDialogues);
                randomDialogues[randDialogues].Play();
                randomDialogeCoolingTime = Time.deltaTime + 60f;

                float clipLenght = randomDialogues[randDialogues].clip.length + Time.deltaTime;
                while(clipLenght > 0)
                {
                    randomDialoguesSubtitles[randDialogues].SetActive(true);
                    clipLenght -= Time.deltaTime;

                }

                randomDialoguesSubtitles[randDialogues].SetActive(false);

            }

            if (randomDialogeCoolingTime > 0)
            {
                randomDialogeCoolingTime -= Time.deltaTime;
            }

        }


    }


    public void enemyInstructions()
    {
        enemyInstructionAudio.Play();
        subtitlePrefab[1].SetActive(true);
    }
    
    public IEnumerator finalInstructions()
    {
        enemyInstructionAudio.Stop();
        subtitlePrefab[1].SetActive(false);
        subtitlePrefab[2].SetActive(true);

        finalInstructionAudio.Play();
        anim.SetBool("isHidden", true);

        yield return new WaitWhile(() => finalInstructionAudio.isPlaying);

        subtitlePrefab[2].SetActive(false);

    }

    public IEnumerator KeyDoorInstructions()
    {
        keyDoorInstructionAudio.Play();
        anim.SetBool("isHidden", false);

        subtitlePrefab[4].SetActive(true);

        yield return new WaitWhile(() => finalInstructionAudio.isPlaying);

        subtitlePrefab[4].SetActive(false);
        subtitlePrefab[3].SetActive(true);

    }

    public IEnumerator BoundingMoments(GameObject subtitle, AudioSource conversation)
    {
        conversation.Play();
        anim.SetBool("isHidden", false);
        subtitle.SetActive(true);

        yield return new WaitWhile(() => conversation.isPlaying);

        subtitle.SetActive(false);
        subtitlePrefab[3].SetActive(true);

    }

    public void helpPlayer()
    {
        helpInstructionAudio.Play();
        anim.SetBool("isHidden", false);

        float pad = 0.5f;
        Vector3 ammoPos = new Vector3(helpBoxPosition.transform.position.x - pad, helpBoxPosition.transform.position.y + pad
            , helpBoxPosition.transform.position.z);
        Vector3 healthPackPos = new Vector3(helpBoxPosition.transform.position.x + pad, helpBoxPosition.transform.position.y + pad
    , helpBoxPosition.transform.position.z);

        Instantiate(ammoPrefab, ammoPos, helpBoxPosition.transform.rotation);
        Instantiate(healthPackPrefab, healthPackPos, helpBoxPosition.transform.rotation);

        GameObject.Find("Subtitles Prefab").gameObject.SetActive(true);
        subtitlePrefab[3].SetActive(true);

    }

    public IEnumerator CompanionSooth()
    {

        int rand = UnityEngine.Random.Range(0, 3);
        yield return new WaitForSeconds(2.5f);


        if (rand == 0)
        {
            soothAudios[0].Play();
            soothSubtitles[0].SetActive(true);

            yield return new WaitWhile(() => soothAudios[0].isPlaying);

            soothSubtitles[0].SetActive(false);
            subtitlePrefab[3].SetActive(true);


        }

        if (rand == 1)
        {
            soothAudios[1].Play();
            soothSubtitles[1].SetActive(true);

            yield return new WaitWhile(() => soothAudios[1].isPlaying);

            soothSubtitles[1].SetActive(false);
            subtitlePrefab[3].SetActive(true);



        }

        if (rand == 2)
        {
            soothAudios[2].Play();
            soothSubtitles[2].SetActive(true);

            yield return new WaitWhile(() => soothAudios[2].isPlaying);

            soothSubtitles[2].SetActive(false);
            subtitlePrefab[3].SetActive(true);


        }


    }

}
