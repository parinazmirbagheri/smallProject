using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionTrigger : MonoBehaviour
{

    [SerializeField] GameObject companion;
    [SerializeField] string keyType;

    private void OnTriggerEnter(Collider other)
    {
        var keychain = other.GetComponent<Keychain>();

        if (other.GetComponent<Keychain>() != null && !other.GetComponent<Keychain>().HaveKey(keyType) && PlayerPrefs.GetInt("isLoaded") == 0)
        {
            StartCoroutine(companion.GetComponent<AnimContr>().KeyDoorInstructions());

        }

        gameObject.GetComponent<BoxCollider>().enabled = false;
    }
}
