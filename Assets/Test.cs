using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{

    public float normalHeight;
    // Start is called before the first frame update
    void Start()
    {
        normalHeight = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(transform.position.x, normalHeight * Mathf.Sin(Time.deltaTime) * 10, transform.position.z);
        print(transform.position.y);
    }
}
