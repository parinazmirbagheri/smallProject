using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerInstructions : MonoBehaviour
{
    [SerializeField] GameObject companion;
    [SerializeField] int enemies;
    //private bool isEnemyKilled = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && PlayerPrefs.GetInt("isLoaded") == 0)
        {
            print(PlayerPrefs.GetInt("isLoaded"));
            companion.GetComponent<AnimContr>().enemyInstructions();
            Destroy(gameObject.GetComponent<BoxCollider>());
        }
    }

    public void EnemyKilled()
    {
        enemies--;

        if(enemies <= 0 && PlayerPrefs.GetInt("isLoaded") == 0)
        {
            StartCoroutine(companion.GetComponent<AnimContr>().finalInstructions());
        }
    }

}
