using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetection : MonoBehaviour
{

    public string enemyType;

    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Player"))
        {
            print("yes");

            if (enemyType.Equals("ShootingGermSlime"))
            {
                StartCoroutine(transform.parent.gameObject.GetComponent<Target>().WanderingAround());
                StartCoroutine(transform.parent.gameObject.GetComponent<Target>().ShootPlayer());

                Debug.Log(gameObject + "Entered");


            }

            else if (enemyType.Equals("GermSpike"))
            {
                StartCoroutine(transform.parent.gameObject.GetComponent<Target>().ShootAllDirection());
                StartCoroutine(transform.parent.gameObject.GetComponent<Target>().WanderingAround());

            }

            else if (enemyType.Equals("GermSlime"))
                StartCoroutine(transform.parent.gameObject.GetComponent<Target>().WanderingAround());

            else if (enemyType.Equals("ExplodingGermSlime"))
                StartCoroutine(transform.parent.gameObject.GetComponent<Target>().FollowTarget());


            //Destroy(gameObject);
            gameObject.GetComponent<SphereCollider>().enabled = false;

        }

        //Destroy(gameObject);


    }

}
