﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class AmmoBox : MonoBehaviour
{
    [AmmoType]
    public int ammoType;
    public int amount;

    public AudioSource audioSource;

    void Reset()
    {
        gameObject.layer = LayerMask.NameToLayer("PlayerCollisionOnly");
        GetComponent<Collider>().isTrigger = true;
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Player"))
        {
            audioSource.Play();
            StartCoroutine(DesolveAmmo(other));

        }

    }

    private IEnumerator DesolveAmmo(Collider other)
    {
        float delay = 0.25f;
        WaitForSeconds wait = new WaitForSeconds(delay);

        Controller c = other.GetComponent<Controller>();

        yield return wait;

        audioSource.Stop();

        if (c != null && !audioSource.isPlaying)
        {
            c.ChangeAmmo(ammoType, amount);
            print("Ammo Pickup" + amount);
            Destroy(gameObject);
        }

    }
}
