﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class Target : MonoBehaviour
{
    public float health = 5.0f;
    public int pointValue;
    public float followSpeed;
    public GameObject player;

    public ParticleSystem DestroyedEffect;

    [Header("Audio")]
    public RandomPlayer HitPlayer;
    public AudioSource IdleSource;

    [Header("Bullet")]
    public GameObject enemyBullet;
    public Transform spawnPoint;
    public float bulletSpeed;
    //[SerializeField] float fireRateThreshold;
    //[SerializeField] float wanderThreshold;
    
    public bool Destroyed => m_Destroyed;
    public bool isInstructionMode;

    private TriggerInstructions _triggerInstructions;

    bool m_Destroyed = false;
    float m_CurrentHealth;

    void Awake()
    {
        Helpers.RecursiveLayerChange(transform, LayerMask.NameToLayer("Target"));
        try
        {
            _triggerInstructions = GameObject.Find("DialogueSystemTrigger").gameObject.GetComponent<TriggerInstructions>();

        }

        catch
        {
            print("No Instruction!");
        }
    }

    void Start()
    {
        if(DestroyedEffect)
            PoolSystem.Instance.InitPool(DestroyedEffect, 16);
        
        m_CurrentHealth = health;
        if(IdleSource != null)
            IdleSource.time = UnityEngine.Random.Range(0.0f, IdleSource.clip.length);

    }


    public IEnumerator ShootPlayer()
    {
        float delay = 1f;
        WaitForSeconds wait = new WaitForSeconds(delay);

        while (health > 0)
        {
            if (delay > 0.5f) delay -= 0.1f;
            yield return new WaitForSeconds(delay);
            GameObject bulletObj = Instantiate(enemyBullet, spawnPoint.transform.position, spawnPoint.transform.rotation) as GameObject;
            bulletObj.SetActive(true);
            Rigidbody bulletRig = bulletObj.GetComponent<Rigidbody>();
            bulletRig.AddForceAtPosition(bulletRig.transform.forward * bulletSpeed, player.transform.position);
            transform.LookAt(player.transform);
            Destroy(bulletObj, 3.5f);
        }
    }

    public IEnumerator ShootAllDirection()
    {
        float delay = 1f;
        WaitForSeconds wait = new WaitForSeconds(delay);

        while (health > 0)
        {
            if (delay > 0.5f) delay -= 0.1f;

            yield return wait;
            GameObject bulletObjR = Instantiate(enemyBullet, spawnPoint.transform.position, spawnPoint.transform.rotation) as GameObject;
            bulletObjR.SetActive(true);
            Rigidbody bulletRigR = bulletObjR.GetComponent<Rigidbody>();
            bulletRigR.AddRelativeForce(bulletRigR.transform.right * bulletSpeed);
            Destroy(bulletObjR, 1f);

            GameObject bulletObjL = Instantiate(enemyBullet, spawnPoint.transform.position, spawnPoint.transform.rotation) as GameObject;
            bulletObjL.SetActive(true);
            Rigidbody bulletRigL = bulletObjL.GetComponent<Rigidbody>();
            bulletRigL.AddRelativeForce(-bulletRigL.transform.right * bulletSpeed);
            Destroy(bulletObjL, 1f);

            GameObject bulletObjF = Instantiate(enemyBullet, spawnPoint.transform.position, spawnPoint.transform.rotation) as GameObject;
            bulletObjF.SetActive(true);
            Rigidbody bulletRigF = bulletObjF.GetComponent<Rigidbody>();
            bulletRigF.AddRelativeForce(bulletRigF.transform.forward * bulletSpeed);
            Destroy(bulletObjF, 1f);

            GameObject bulletObjB = Instantiate(enemyBullet, spawnPoint.transform.position, spawnPoint.transform.rotation) as GameObject;
            bulletObjB.SetActive(true);
            Rigidbody bulletRigB = bulletObjB.GetComponent<Rigidbody>();
            bulletRigB.AddRelativeForce(-bulletRigB.transform.forward * bulletSpeed);
            transform.LookAt(player.transform);
            Destroy(bulletObjB, 1f);
        }
    }

    public IEnumerator WanderingAround()
    {
        float delay = 1f;
        WaitForSeconds wait = new WaitForSeconds(delay);

        float Z = transform.position.z;
        float Y = transform.position.y;
        float X = transform.position.x;

        while (health > 0)
        {
            yield return wait;

            if (delay > 0.7f) delay -= 0.1f;

            float newZ = UnityEngine.Random.Range(Z - 2, Z + 2);
            float newY = UnityEngine.Random.Range(Y - 2, Y + 2);
            float newX = UnityEngine.Random.Range(X - 2, X + 2);
            transform.GetComponent<Rigidbody>().MovePosition(new Vector3(newX, newY, newZ));
            transform.position = Vector3.Lerp(new Vector3(transform.position.x, transform.position.y, transform.position.z),
                new Vector3(newX, newY, newZ), 0.5f);
            transform.LookAt(player.transform);

        }
    }

    public IEnumerator FollowTarget()
    {
        float delay = 1f;
        WaitForSeconds wait = new WaitForSeconds(delay);

        while (health > 0)
        {
            yield return wait;

            if (delay > 0.7f) delay -= 0.1f;


            transform.LookAt(player.transform);

            //get the distance between the chaser and the target
            float distance = Vector3.Distance(transform.position, player.transform.position);

            //so long as the chaser is farther away than the minimum distance, move towards it at rate speed.
            if (distance > 0)
                transform.position += transform.forward * followSpeed * Time.deltaTime;

        }

    }

    public void Got(float damage)
    {
        m_CurrentHealth -= damage;
        
        if(HitPlayer != null)
            HitPlayer.PlayRandom();
        
        if(m_CurrentHealth > 0)
            return;

        Vector3 position = transform.position;
        
        //the audiosource of the target will get destroyed, so we need to grab a world one and play the clip through it
        if (HitPlayer != null)
        {
            var source = WorldAudioPool.GetWorldSFXSource();
            source.transform.position = position;
            source.pitch = HitPlayer.source.pitch;
            source.PlayOneShot(HitPlayer.GetRandomClip());
        }

        if (DestroyedEffect != null)
        {
            var effect = PoolSystem.Instance.GetInstance<ParticleSystem>(DestroyedEffect);
            effect.time = 0.0f;
            effect.Play();
            effect.transform.position = position;
        }

        m_Destroyed = true;
        
        gameObject.SetActive(false);
       
        GameSystem.Instance.TargetDestroyed(pointValue);

        if (isInstructionMode) _triggerInstructions.EnemyKilled();

    }
}
