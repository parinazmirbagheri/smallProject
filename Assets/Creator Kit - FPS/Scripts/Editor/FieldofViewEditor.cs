using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(FieldofView))]
public class FieldofViewEditor : Editor
{
    private void onSceneGUI()
    {
        FieldofView fov = (FieldofView)target;
        Handles.color = Color.white;
        Handles.DrawWireArc(fov.transform.position, Vector3.up, Vector3.forward, 360, fov.radius);

        Vector3 angleView01 = DirectionFromAngle(fov.transform.eulerAngles.y, -fov.angle / 2);
        Vector3 angleView02 = DirectionFromAngle(fov.transform.eulerAngles.y, fov.angle / 2);

        Handles.color = Color.yellow;
        Handles.DrawLine(fov.transform.position, fov.transform.position + angleView01 * fov.radius);
        Handles.DrawLine(fov.transform.position, fov.transform.position + angleView02 * fov.radius);


    }

    private Vector3 DirectionFromAngle(float eulerY, float angleInDegrees)
    {
        angleInDegrees += eulerY;

        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));

    }
}
