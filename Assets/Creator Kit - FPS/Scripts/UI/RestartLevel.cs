﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartLevel : MonoBehaviour
{
    public void Restart()
    {

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        PlayerPrefs.SetInt("isLoaded", 1);
        //GameSystem.Instance.CompanionSooth();
        Time.timeScale = 1;
    }

    public void NextLevel()
    {
        UIAudioPlayer.PlayPositive();
        GameSystem.Instance.NextLevel();
        Time.timeScale = 1;
    }
}
