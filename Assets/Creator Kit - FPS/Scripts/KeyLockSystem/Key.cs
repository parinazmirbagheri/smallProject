﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Key : MonoBehaviour
{
    public string keyType;
    public Text KeyNameText;

    [Header("Audio")]
    [SerializeField] AudioSource PickUpAudio;

    private void Update()
    {
        transform.Rotate(new Vector3(0, 50, 0) * Time.deltaTime);
    }

    void OnEnable()
    {
        KeyNameText.text = keyType;
    }

    void OnTriggerEnter(Collider other)
    {
        PickUpAudio.Play();
        StartCoroutine(DesolveKey(other));

    }

    private IEnumerator DesolveKey(Collider other)
    {
        float delay = 0.25f;
        WaitForSeconds wait = new WaitForSeconds(delay);
        var keychain = other.GetComponent<Keychain>();

        yield return wait;

        PickUpAudio.Stop();

        if (keychain != null)
        {
            keychain.GrabbedKey(keyType);
            Destroy(gameObject);
        }

    }
}
