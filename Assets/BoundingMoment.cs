using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundingMoment : MonoBehaviour
{
    [SerializeField] GameObject subtitle;
    [SerializeField] AudioSource conversation;
    [SerializeField] GameObject companion;

    public void AcidFood()
    {
        StartCoroutine(companion.GetComponent<AnimContr>().BoundingMoments(subtitle, conversation));
    }

    private void OnTriggerEnter(Collider other)
    {
        var keychain = other.GetComponent<Keychain>();

        if (other.GetComponent<Keychain>() != null && other.GetComponent<Keychain>().HaveKey("StomachKey") && PlayerPrefs.GetInt(gameObject.name) == 0)
        {

            PlayerPrefs.SetInt(gameObject.name, 1);
            AcidFood();
            gameObject.GetComponent<BoxCollider>().enabled = false;

        }

    }
}
