using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{

    [HideInInspector]
    public bool isCompanionModeActive = false;

    public Toggle companionModeToggle;

    private void Awake()
    {
        PlayerPrefs.SetInt("isLoaded", 0);
        PlayerPrefs.SetInt("AcidFood", 0);
        PlayerPrefs.SetInt("FatPatient", 0);
        PlayerPrefs.SetInt("LittleHeart", 0);
        PlayerPrefs.SetInt("GoldTooth", 0);

    }

    public void LoadCompanionScene()
    {
        if (isCompanionModeActive) SceneManager.LoadScene(1);
    }

    public void LoadNormalScene()
    {
        if (!isCompanionModeActive) SceneManager.LoadScene(2);

    }

    public void LoadStartingLevel()
    {
        if(companionModeToggle.isOn) SceneManager.LoadScene(1);

        else if(!companionModeToggle.isOn)   SceneManager.LoadScene(2);
    }
}
