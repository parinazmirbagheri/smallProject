using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableQuestionnaire : MonoBehaviour
{

    [SerializeField] GameObject questionnaireUI;

    public void Disable()
    {
        questionnaireUI.SetActive(false);
        //GameObject.Find("QuestionnaireUI").SetActive(false);

    }
}
